<?php
return [
	"basePath"=>"frame/public/",
	"rootDir"=>dirname(dirname(__FILE__)),
	"layout"=>[
		"auth"=>"layouts/admin/auth",
		"admin"=>"layouts/admin/layoutAdministrator",
		"home"=>"layouts/home/page"
	],
	"database"=>[
		"hostname"=>"127.0.0.1",
		"dbname"=>"bookstore_2",
		"username"=>"root",
		"password"=>""
	]
];