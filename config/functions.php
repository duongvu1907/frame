<?php 
require dirname(__FILE__).'/library/PHPMailer/src/Exception.php';
require dirname(__FILE__).'/library/PHPMailer/src/PHPMailer.php';
require dirname(__FILE__).'/library/PHPMailer/src/SMTP.php';
require dirname(__FILE__).'/library/PHPMailer/src/OAuth.php';
require dirname(__FILE__).'/library/PHPMailer/src/POP3.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

function str_to_datetime($str){
	return date("Y-m-d h:i:s",strtotime($str));
}
function url($uri){
	GLOBAL $config;
	return "http://".$_SERVER["HTTP_HOST"]."/".$config["basePath"].$uri;
}
function assets($dir){
	GLOBAL $config;
	return "http://".$_SERVER["HTTP_HOST"]."/".$config["basePath"].$dir;
}
function vn_to_str($str){

	$unicode = array(

		'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',

		'd'=>'đ',

		'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',

		'i'=>'í|ì|ỉ|ĩ|ị',

		'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',

		'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',

		'y'=>'ý|ỳ|ỷ|ỹ|ỵ',

		'A'=>'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',

		'D'=>'Đ',

		'E'=>'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',

		'I'=>'Í|Ì|Ỉ|Ĩ|Ị',

		'O'=>'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',

		'U'=>'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',

		'Y'=>'Ý|Ỳ|Ỷ|Ỹ|Ỵ',

	);

	foreach($unicode as $nonUnicode=>$uni){

		$str = preg_replace("/($uni)/i", $nonUnicode, $str);

	}
	$str = str_replace(' ','-',$str);
	$str = str_replace('--','',$str);
	$str = str_replace('---','',$str);

	return $str;
}
function name_to_slug($name,$id){
	return  strtolower(vn_to_str($name)."-".$id);
}
function getID($slug){
	$str = explode("-",$slug);
	return (int)$str[count($str)-1];
}
function str_random($n) { 
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
	$randomString = ''; 

	for ($i = 0; $i < $n; $i++) { 
		$index = rand(0, strlen($characters) - 1); 
		$randomString .= $characters[$index]; 
	} 

	return $randomString; 
} 
function sendMail($toEmail,$genre,$subject,$data=array()){
	$mail = new PHPMailer;
	$mail->CharSet = "UTF-8";
//Tell PHPMailer to use SMTP
	$mail->isSMTP();

	$mail->SMTPDebug = 0;

	$mail->Host = 'smtp.gmail.com';

	$mail->Port = 587;

	$mail->SMTPSecure = 'tls';

	$mail->SMTPAuth = true;

	$mail->Username = "qmstowertohuu@gmail.com";

	$mail->Password = "nhoxcoz1";

	$mail->setFrom('qmstowertohuu@gmail.com', 'ABC Store');

	$mail->isHTML(true);
	// $mail->addReplyTo('duongvu.survive@gmail.com', 'First Last');

	$mail->addAddress($toEmail,'Administrator');

	$mail->Subject = $subject;
	$data["subject"] = $mail->Subject;
	$data["fromEmail"] = "qmstowertohuu@gmail.com";
	$data["info"] = "Send from : ".$_SERVER["REMOTE_ADDR"];
	$mail->Body    = extract_file($genre,$data);
	// var_dump(extract_file("vefiry",$data));
	if (!$mail->send()) {
		return false;
	} else {
		 return true;
	}
}
	function extract_file($file,$data=array()){
		GLOBAL $config;
		$file = $config["rootDir"]."/app/views/email/".$file.".php";
		if(file_exists($file)) {
			empty($data)?"":extract($data);
			ob_start();
			require $file;
			$content = ob_get_contents();
			ob_clean();
			ob_end_flush();
			return $content;
		}
		return ;
	}
