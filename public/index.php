<?php
	session_start();
	date_default_timezone_set("Asia/Ho_Chi_Minh");
	require_once dirname(__FILE__)."/../app/core/App.php";
	$config = require_once dirname(__FILE__)."/../config/main.php";
	require_once dirname(__FILE__)."/../config/functions.php";
	$app = new App($config);
	$app->run();
	