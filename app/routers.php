<?php
    use app\core\Controller;

// ==========START Route Administrator ==================
    Router::get("/admin",function(){
        $controller = new Controller;
        return $controller->redirect(url('admin/login'));
    },true);
    Router::get("/admin/dashboard","admin\DashboardController@index",true);
    Router::get("/admin/customer/d/{id}","admin\DashboardController@delete_customer",true);


    Router::get("/admin/login","auth\AdminController@login");
    Router::get("/admin/logout","auth\AdminController@logout");
    Router::post("/admin/login","auth\AdminController@do_login");
    Router::get("/admin/login/forget-password","auth\AdminController@forgetPassword");
    Router::post("/admin/login/forget-password","auth\AdminController@send_token");
    Router::get("/admin/login/verify/{id}","auth\AdminController@check_token");
    Router::post("/admin/login/verify/{id}","auth\AdminController@checked_token");

    Router::get("/admin/login/change/{id}","auth\AdminController@change_password");
    Router::post("/admin/change_password/{id}","auth\AdminController@change_password");

    Router::get("/admin/register","auth\AdminController@register");
    Router::post("/admin/register","auth\AdminController@do_register");
    Router::get("/admin/profile","auth\AdminController@profile");
    Router::post("/admin/profile","auth\AdminController@save_profile",true);

    Router::get("/admin/search","auth\AdminController@search",true);
    Router::post("/admin/search/input","auth\AdminController@search_input",true);

    Router::post("/admin/authors/ajax","admin\AuthorController@ajax",true);
    Router::post("/admin/authors/add","admin\AuthorController@do_add",true);
    Router::get("/admin/authors","admin\AuthorController@index",true);
    Router::get("/admin/authors/delete/{id}","admin\AuthorController@delete",true);
    Router::post("/admin/authors/edit","admin\AuthorController@do_edit",true);

    Router::post("/admin/genres/ajax","admin\GenreController@ajax",true);
    Router::post("/admin/genres/add","admin\GenreController@do_add",true);
    Router::get("/admin/genres","admin\GenreController@index",true);
    Router::post("/admin/genres/edit","admin\GenreController@do_edit",true);
    Router::get("/admin/genres/delete/{id}","admin\GenreController@delete",true);

    Router::post("/admin/genres/ajax","admin\GenreController@ajax",true);
    Router::post("/admin/genres/add","admin\GenreController@do_add",true);
    Router::get("/admin/genres","admin\GenreController@index",true);
    Router::post("/admin/genres/edit","admin\GenreController@do_edit",true);
    Router::get("/admin/genres/delete/{id}","admin\GenreController@delete",true);

    Router::post("/admin/publishers/ajax","admin\PublisherController@ajax",true);
    Router::post("/admin/publishers/add","admin\PublisherController@do_add",true);
    Router::get("/admin/publishers","admin\PublisherController@index",true);
    Router::post("/admin/publishers/edit","admin\PublisherController@do_edit",true);
    Router::get("/admin/publishers/delete/{id}","admin\PublisherController@delete",true);

    Router::get("/admin/product","admin\ProductController@index",true);
    Router::get("/admin/product/add","admin\ProductController@add");
    Router::get("/admin/product/edit/{id}","admin\ProductController@edit",true);
    Router::get("/admin/product/delete/{id}","admin\ProductController@delete",true);
    Router::post("/admin/product/ajax","admin\ProductController@ajax",true);

    Router::post("/admin/product/edit/{id}","admin\ProductController@do_edit",true);
    Router::post("/admin/product/add","admin\ProductController@do_add",true);

    Router::get("/admin/customize","admin\CustomizeController@index",true);
    Router::get("/admin/customize/slider/d/{id}","admin\CustomizeController@delete",true);
    Router::get("/admin/customize/features-books/d/{id}","admin\CustomizeController@delete_fb",true);
    Router::post("/admin/customize/features-books/add","admin\CustomizeController@add_features_books",true);

    Router::post("/admin/customize/slider/add","admin\CustomizeController@add_slider",true);
    Router::post("/admin/customize/slider/ajax","admin\CustomizeController@ajax",true);
    Router::post("/admin/customize/slider/edit","admin\CustomizeController@edit",true);


    Router::get("/admin/discounts","admin\DiscountController@index",true);
    Router::get("/admin/discounts/d/{id}","admin\DiscountController@delete",true);
    Router::get("/admin/discounts/mixed","admin\DiscountController@mixed",true);

    Router::post("/admin/discounts/mixed","admin\DiscountController@do_mixed",true);
    
    Router::post("/admin/discounts-books/add","admin\DiscountController@discounts_books",true);
    Router::post("/admin/discounts-customers/add","admin\DiscountController@discounts_customers",true);

    Router::post("/admin/discounts/add","admin\DiscountController@add",true);
    Router::post("/admin/discounts/ajax","admin\DiscountController@ajax",true);
    Router::post("/admin/discounts/ajax_name","admin\DiscountController@ajax_name",true);
    Router::post("/admin/discounts/edit","admin\DiscountController@edit",true);

    Router::post("/admin/discounts/statistics", "admin\DiscountController@statistics",true);
    Router::get("/admin/discounts/mixed/d/{filter}/{id}","admin\DiscountController@delete_mixed",true);
    Router::get("/orders/active/{id}","admin\DashboardController@active_order");
    Router::post("/orders/active","admin\DashboardController@save_order");
    
// ==========END Route Administrator ==================

// ==========Start Route Home Page ==================
    Router::get("","home\HomeController@index");
    Router::get("/chi-tiet/{slug}","home\HomeController@detail");

    Router::get("/dang-nhap","home\CustomerController@login");
    Router::post("/dang-nhap","home\CustomerController@do_login");
    Router::get("/dang-ky","home\CustomerController@register");
    Router::post("/dang-ky","home\CustomerController@do_register");
    Router::get("/u/xac-minh","home\CustomerController@forget");
    Router::get("/u/logout","home\CustomerController@logout");
    Router::get("/u/ho-so","home\CustomerController@profile");
    Router::post("/u/xac-minh","home\CustomerController@send_forget");
    Router::post("/u/ho-so/edit","home\CustomerController@edit");

    Router::get("/u/gio-hang","home\HomeController@cart");
    Router::post('/add/cart',"home\HomeController@add_cart");
    Router::get('/cart/delete/{id}',"home\HomeController@delete");
    Router::post('/sach/danh-gia',"home\CustomerController@comment");
    Router::post('/u/gio-hang/luu',"home\HomeController@save_cart");

    Router::get('/u/gio-hang/phan-hoi','home\HomeController@report');

    Router::get('/the-loai/{slug}',"home\HomeController@genres");
    Router::get('/nha-xuat-ban/{slug}',"home\HomeController@publishers");
    Router::get('/tac-gia/{slug}',"home\HomeController@authors");
    Router::post('/sp/ajax','home\HomeController@ajax');
    Router::get('/sach-moi-nhat','home\HomeController@books');
	
// ==========END Route Home Page ==================

    Router::post("/customers/location/province","home\CustomerController@get_districts_ajax");
    Router::post("/customers/location/district","home\CustomerController@get_communes_ajax");

    Router::any("*","home\HomeController@_404");