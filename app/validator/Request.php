<?php  
/**
 * summary
 */
namespace App\validator;
use app\database\DB;
class Request
{
    
    protected $headers;
    protected $request;
    protected $method;
    protected static $error=[];
    protected static $staticRules = [
    	"required"=>[
    		"rule"=>"required",
    		"message"=>"không để trống"
    	],
    	"min"=>[
    		"rule"=>"min",
    		"value"=>0,
    		"message"=>"This Field is less min"
    	],
    	"max"=>[
    		"rule"=>"max",
    		"value"=>0,
    		"message"=>"This Field is over max"
    	],
        "email"=>[
            "rule"=>"email",
            "message"=>"định dạng email"
        ],
        "unique"=>[
            "rule"=>"unique",
            "message"=>"dã tồn tại"
        ]

    ];
    public function __construct()
    {
    	$this->method = $_SERVER["REQUEST_METHOD"];
    	$this->setParams($this->method);
    	$this->reference = isset($_SERVER["HTTP_REFERER"])?$_SERVER["HTTP_REFERER"]:"";
    	// $this->setHeaders();

    }
    public function setParams($method){
    	$params = array();
        $files = array();
    	switch ($method) {
    		case "GET":
    			$params = $_GET;
    			break;
    		case "POST":
    			$params = $_POST;
                if (!is_null($_FILES)) {
                    $files = $_FILES;
                }
    			break;
    	}
    	foreach ($params as $key => $value) {
    		if ($value != "" && !is_null($value)) {
    			$this->$key = $value;
    		}
    	}
        foreach ($files as $key => $value) {
            $this->$key = $value; 
        }
    }
    // public function setFile(){
    //     if (!is_null($_FILES)) {
    //         $file = $_FILES;
    //         foreach ($file as $key => $value) {
    //             if ($value != "" && !is_null($value)) {
    //                 $this->$key = $value;
    //             }
    //         }
    //     }
    // }
    public function setHeaders(){
       foreach ($_SERVER as $name => $value) 
       { 

           if (substr($name, 0, 5) == 'HTTP_') 
           { 
           	echo $value;
               $this->headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value; 
           } 
       } 
    }
    public function __set($key,$param){
    	$this->request[$key] = $param;
    }
    public function __get($key){

    	if (!isset($this->request[$key])) {
    		return null;
    	}
    	return $this->request[$key];
    }
    public function validated($rules = array()){
    	foreach ($rules as $key => $rule) {
    		$this->parseRules($key,$rule);
    	}
        return self::$error;
    }
    private function parseRules($key,$rule){
    		$rule = strpos($rule,"|")?explode("|", $rule):$rule;
    		if (is_array($rule)) {
    			foreach ($rule as $r) {
    				$this->actionLaw($key,$r);
    			}
    		}else{
    			 	$this->actionLaw($key,$rule);
    		}		
    }
    private function actionLaw($key,$value){
    	if (strpos($value,"min")===0 || strpos($value,"max")===0) {
    		$m = explode(":",$value);
    		$value = $m[0];
    	}
        // echo strpos($value,"unique");
        if (strpos($value,"unique")===0) {

            $m = explode(":",$value);  
            $value = $m[0];
            $table = $m[1];
        }
    	// echo $value;
    	switch (self::$staticRules[$value]["rule"]) {
    		case "required":

    			!is_null($this->$key)?$this->$key:array_push(self::$error,$key." ".self::$staticRules[$value]["message"]);
    			// var_dump($this->error);
    			break;
    		case "min":
    				if (strlen($this->$key)<$m[1]) {
    					array_push(self::$error,$key." ".self::$staticRules[$value]["message"]);
    				}
    			break;
            case "email":
                    if (!filter_var($this->$key,FILTER_VALIDATE_EMAIL)){
                        array_push(self::$error,$key." ".self::$staticRules[$value]["message"]);
                    }
                break;
            case "unique":
                    $user = DB::table($table)->where($key,"LIKE","\"%".$this->$key."%\"")->first();
                    if ($user) {
                        array_push(self::$error,$key." ".self::$staticRules[$value]["message"]);
                    }
                break;
    	}
    }
}