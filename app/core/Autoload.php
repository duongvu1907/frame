<?php

use app\core\AppException;

class Autoload
{
	private $rootDir ;
	function __construct($rootDir)
	{
		$this->rootDir = $rootDir;
		spl_autoload_register([$this,"autoload"]);
		$this->autoloadFile();
	}
	private function autoload($class){

		$filePath = $this->rootDir."\\".$class.".php";
		if (file_exists($filePath)) {
			require_once $filePath;
		}else{
			// echo $filePath;
			 throw new AppException("$filePath Not Found");
		}
	}
	private function autoloadFile(){
		foreach ($this->DefaultFileLoader() as $file) {
			if (file_exists($this->rootDir."/".$file)) {
				require_once $this->rootDir."/".$file;
			}else{
				throw new AppException("$file Not Found");
			}
		}
	}
	private function DefaultFileLoader(){
		return [
			"app/core/Router.php",
			"app/routers.php"
		];
	}
}