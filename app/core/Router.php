<?php

	use app\core\Registry;
	use app\core\AppException;
	use app\core\Abort;
	class Router
	{
		private $params = [];
		private static $routers = [];
		private static $basePath;
		function __construct($basePath)
		{
			self::$basePath = $basePath;
		}
		private function getRequestUrl(){
			$uri = isset($_SERVER["REQUEST_URI"])?$_SERVER["REQUEST_URI"]:"/";
			$url = str_replace(self::$basePath,'', $uri);
			$url  = $url === '' || empty($url)?'/':$url;
			return $url;
		}
		private function getRequestMethod(){
			$method = isset($_SERVER["REQUEST_METHOD"])?$_SERVER["REQUEST_METHOD"]:"GET";
			return $method;
		}
		public static function addRouter($method,$url,$action,$middleware){
			self::$routers[] = [$method,$url,$action,$middleware];
		}
		public static function get($url,$action,$middleware=false){
			self::addRouter("GET",$url,$action,$middleware);
		}
		public static function post($url,$action,$middleware=false){
			self::addRouter("POST",$url,$action,$middleware);
		}
		public static function any($url,$action,$middleware=false){
			self::addRouter("GET|POST",$url,$action,$middleware);
		}

		public function map(){
			$checkRouter = false;
			$requestUrl = rtrim(explode("?",$this->getRequestUrl())[0],"/");
			$requestMethod = $this->getRequestMethod();
			// echo $requestUrl;
			foreach (self::$routers as $router) {
				list($method,$url,$action,$middleware) = $router;
				// echo $method."<br>";

				if (strpos($method, $requestMethod) === FALSE) {
					continue;
				}
				// echo $url." => ".$requestUrl."=>".strcmp(strtolower($url), strtolower($requestUrl))."<br>";
				// if (strcmp(strtolower($url), strtolower($requestUrl)) != 0) {
				// 	continue;
				// }
				if ($url === '*'){
					$checkRouter = true;
				}
				elseif (strpos($url, '{') === FALSE) {
					// echo $requestUrl."--".$url." : ".strcmp(strtolower($url), strtolower($requestUrl))."<br>";
					if (strcmp(strtolower($url), strtolower($requestUrl)) === 0 ) {
						$checkRouter = true;

					}else{
						$checkRouter = false;
						continue;
					}
				}
				elseif (strpos($url, '}') === FALSE) {
					continue;
				}
				else{
					// var_dump($checkRouter);
					$routerParams = explode('/', $url);
					$requestParams = explode('/', $requestUrl);
					if (count($routerParams) !== count($requestParams)) {
						continue;
					}
					$urlTest = $requestParams;
					$routeTest = $routerParams;
					$count = [];
					foreach ($routerParams as $key => $value) {
						if (preg_match('/^{\w+}$/', $value)) {
							array_push($count,$key);
							unset($routeTest[$key]);
						}
					}
					// var_dump($requestParams);
					
					for($i=0;$i<count($count);$i++){
						if (count($urlTest)>$count[$i]) {
							unset($urlTest[$count[$i]]);
						}
					}
					if (!empty(array_diff($routeTest,$urlTest))) {
						// echo "<pre>";
						continue;
					}
					
					foreach ($routerParams as $key => $rParam) {
						if (preg_match('/^{\w+}$/', $rParam)) {
							$quest = explode("?", $requestParams[$key]);
							$this->params[] = $quest[0]; 
						}
					}
					// var_dump($this->params);
					$checkRouter = true;
				
				}
				if ($checkRouter === true) {
					if (!$middleware) {
							if (is_callable($action)) {
							call_user_func_array($action, $this->params);
								
							}elseif (is_string($action)) {
								$this->compileController($action,$this->params);
							}
					}else{
						$this->middleware();
							// echo $action;
							if (is_callable($action)) {
								call_user_func_array($action, $this->params);

							}elseif (is_string($action)) {
								$this->compileController($action,$this->params);
							}
					}
					return;
				}else{
					continue;
				}

			}
			return;
		}
		private function compileController($action,$params){
			// Abort::HTTP_CODE(404,"Not Found ALITA");
			if (count(explode('@', $action))!== 2) {
				throw new AppException("Wrong Router $action");
			}
			$className = explode('@',$action)[0];
			$methodName = explode('@', $action)[1];
			Registry::getInstance()->controller = $className;
			$className = "app\\controllers\\".$className;
			// echo $className;
			if (class_exists($className)) {
				Registry::getInstance()->action = $methodName;
				$object = new $className;
				
				if (method_exists($object, $methodName)) {
					call_user_func_array([$object,$methodName], $params);
				}else{
					throw new AppException("Class $className have not function : $methodName");
				}
			}else{
				throw new AppException("Dont found class : ".$className);
			}

		}
		public function run(){
			$this->map();
		}
		public function middleware(){
			if (!isset($_SESSION["email"]) || is_null($_SESSION["email"])) {
				header("Location: ".url('admin/login'));
			}
		} 
	}