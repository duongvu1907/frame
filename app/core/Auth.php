<?php 
namespace app\core;
use app\core\AppException;
use app\database\DB;
class Auth
{
	protected static $email;
	protected static $username;
	public static function user(){
		self::$email = isset($_SESSION["email"])?$_SESSION["email"]:"";
		if (empty(self::$email)) {
			throw new AppException('Auth is not exists',404);
		}
		return DB::table("users")->where("email","LIKE","\"".self::$email."\"")->first();
	}
	public static function customer(){
		self::$username = isset($_SESSION["username"])?$_SESSION["username"]:"";
		if (empty(self::$username)) {
			throw new AppException('Auth is not exists',404);
		}
		return DB::table("customers")->where("username","LIKE","\"".self::$username."\"")->first();
	}

}