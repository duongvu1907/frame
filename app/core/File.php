<?php 

 /**
  * 
  */
 namespace app\core;
 use app\core\Registry;
 class File
 {
 	protected static $rootDir;
 	function __construct()
 	{
 		self::$rootDir = Registry::getInstance()->config["rootDir"]."/public/";
 	}
 	public static function folder_exists($dir){
		if (realpath($dir) && is_dir($dir)) {
			return true;
		}
		return false;
	}
	public static function move($file,$dir){
		new self;
		$path = self::$rootDir.$dir;
		// echo $path;
		// var_dump(self::folder_exists($path));
		if (self::folder_exists($path)) {
			$name  = self::name_file($file["name"],$path);
			// echo $name;
			move_uploaded_file($file["tmp_name"],$dir."/".$name);
			return $name;
		}
		return false;
	}
	public static function name_file($name,$dir){
		$file = $dir."/".$name;
		if (file_exists($file)){
			$dd = explode(".",$name);
			$type= $dd[count($dd)-1];
			$name = str_replace($type,"",$name);
			$name .= str_random(5).".".$type;
		}
		
		return $name;
	}
	public static function delete($file){
		new self;
		$path = self::$rootDir.$file;
		if (file_exists($path)) {
			unlink($path);
		}
	}
 }