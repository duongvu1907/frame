<?php
namespace app\core;
use \App;
use app\core\Registry;
use app\database\DB;
class Controller
{
	private $layout = null;
	private $rootDir;
	function __construct()
	{
		
		$this->rootDir = Registry::getInstance()->config["rootDir"];
	}
	
	public function redirect($url,$isEnd=true,$responseCode = 302){
		header("Location:".$url,$isEnd,$responseCode);
		if ($isEnd) {
			die();
		}
	}
	public function RenderContent($view,$data = array()){
		
		if (strpos($view,".")) {
			$path = explode(".",$view);
			$view="";
			foreach ($path as $p) {
				$view.="/".$p;
			}
		}

		$file = $this->rootDir.'/app/views'.$view.'.php';
		if (file_exists($file)) {
			empty($data)?"":extract($data);
			ob_start();
			require $file;
			$content = ob_get_contents();
			ob_clean();
			ob_end_flush();
			return $content;
		}
		return;
	}
	public function view($view,$data = array(),$is_debug=null){
		
		if ($is_debug!=null) {
			$file = $this->rootDir.'/app/views/exception/'.$view.'.php';
				
				if (file_exists($file)) {
					require $file;
				}
		}else{
			$content = $this->RenderContent($view, $data);
			if($this->layout != null){
				$file = $this->rootDir.'/app/views/'.$this->layout.'.php';
				
				if (file_exists($file)) {
					require $file;
				}
			}
		}
	}
	public function layout($layout){
		$this->layout = Registry::getInstance()->config['layout'][$layout];
	}
}