<?php

namespace app\core;
use app\core\AppException;
class Abort
{
	protected static $view;
	protected static $log;
	public function __construct($view){
		self::$log = $view;
		$view = Registry::getInstance()->config["rootDir"].$view;
		if (Registry::getInstance()->file->folder_exists($view)) {
			self::$view = $view;
		}
	}
	public static  function HTTP_CODE($code,$message){
		if (empty(self::$view)) {
			throw new AppException("The Abort has not yet initialized Or ".self::$log." not found",500);
		}
		require  self::$view."/$code.php";
	}

}