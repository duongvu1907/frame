<?php
	namespace app\core;
	use \Exception;
	use app\core\Registry;
	class AppException extends Exception
	{
		protected $controller;
		protected $dir;
		function __construct($message,$code=null)
		{
			$controller = new Controller;
			$this->dir =  Registry::getInstance()->config["rootDir"];
			set_exception_handler([$this,"error_handle"]);
			parent::__construct($message,$code);
		}
		public function error_handle($exception){
			// var_dump(Registry::getInstance()->config);
			require $this->dir.'\app\views\exception\exception.php';
		}
	}