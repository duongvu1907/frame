<?php
/**
 * 
 */
require_once dirname(__FILE__)."/Autoload.php";
require_once dirname(__FILE__)."/Registry.php";
use app\core\Registry;
use app\core\File;
use app\core\Abort;
class App
{
	protected $router;
	private static $controller;
	private static $action;
	function __construct($config)
	{
		Registry::getInstance()->config = $config;
		new Autoload($config["rootDir"]);
		$this->router = new Router($config['basePath']);
		Registry::getInstance()->file = new File;
		new Abort("/app/views/http_code/");
	}
	
	public function run(){
		$this->router->run();
	}

}