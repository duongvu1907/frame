<?php
/**
 * 
 */
namespace app\core;
class Registry
{
	private static $instance = null;
	private $storage;
	private function __construct()
	{
		
	}
	public static function getInstance(){
		if (self::$instance==null) {
			self::$instance = new Registry;
		}
			return self::$instance;
		// return null;
	}
	public function __set($name,$value){
		$this->storage[$name] = $value;
	}
	public function __get($name){
		if (isset($this->storage[$name])) {
			return $this->storage[$name];
		}
		return null;
	}
	

}