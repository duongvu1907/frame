<?php use app\database\DB; ?>
<div class="row">
	<div class="col-12 customer-pad ">
		<div class="card">
			<div class="card-header">
				<h4>Thông tin khach hang</h4>
			</div>
			<div class="card-body">
				
				<ul>
					<li>Tên : <?php echo $user->name ?></li>
					<li>Tài khoản : <?php echo $user->username ?></li>
					<li>Email:  <?php echo $user->email ?></li>
					<li>SĐT : <?php echo $user->phone ?></li>
					<li>Địa chỉ : <?php echo $order->address ?></li>
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-12 customer-pad ">
		<div class="card">
			<div class="card-header">
				<h4>Thông tin đơn hàng MS <?php echo $order->id ?></h4>
				<br>
				<p><?php echo $order->created_at ?></p>
			</div>
			<div class="card-body">
				<table class="table">
					<tr>
						<th>#</th>
						<th>Tên sách</th>
						<th>Số lượng</th>
						<th>Đơn giá</th>
					</tr>
					<?php $total=0; ?>
					<?php foreach ($orders_details as $od): ?>
						<?php $book = DB::table("books")->where("id","=",$od->book_id)->first() ?>
						<tr>
							<?php $total+=$od->quantity*$od->price ?>
							<td><?php echo $od->id ?></td>
							<td><?php echo $book->name ?></td>
							<td><?php echo $od->quantity ?></td>
							<td><?php echo number_format($od->price) ?></td>
						</tr>
					<?php endforeach ?>
				</table>
				<?php if($order->status==0){ ?><div style="padding: 20px"></div>
				<form action="<?php echo url('orders/active') ?>" method="post">
					<input type="number" value="<?php echo $order->id ?>" name="order_id" hidden>
					<input type="number" value="<?php echo $total ?>" name="amount" hidden>
				<button class="btn btn-primary">Active</button>
				</form>
			<?php } ?>
			</div>
		</div>
	</div>
</div>