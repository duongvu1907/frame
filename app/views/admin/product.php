<?php use app\database\DB; ?>
<div class="row" style="margin-top: 50px">
	<!-- <div class="col-md-1"></div> -->
	<div class="col-md-12 customer-bg customer-pad">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<a href="<?php echo url('admin/product/add') ?>" class="text-white btn btn-primary" style="width: 100%">THÊM SẢN PHẨM</a>
			</div>
			<div class="col-md-2"></div>
		</div>
		<div class="row" style="margin-top: 50px">
			<div class="col-md-12">
				<div class="table-container">
				<table class="table table-hover">
					<thead>
						<th>#</th>
						<th>Ảnh</th>
						<th>Tên</th>
						<th>Tác giả</th>
						<th>NXB</th>
						<th>Giá</th>
						<th>Số lượng </th>
						<th>#</th>
					</thead>
					<tbody>
						<?php foreach ($books as $book): ?>
							<tr>
								<td><?php echo $book->id ?></td>
								<td><img src="<?php echo assets('upload/books/'.$book->image) ?>" width="120px"></td>
								<td><?php echo $book->name ?></td>
								<td><?php echo DB::table("authors")->where("id","=",$book->author_id)->first()->name ?></td>
								<td><?php echo DB::table("publishers")->where("id","=",$book->publisher_id)->first()->name ?></td>
								<td><?php echo number_format($book->price) ?></td>
								<td><?php echo $book->quantity ?></td>
								<td>
									<a href="<?php echo url('admin/product/edit/'.$book->id) ?>"><span class="fa fa-edit customer-icon-sm"></span></a> &nbsp;
									<a onclick="return confirm('Xoa ?')" href="<?php echo url('admin/product/delete/'.$book->id) ?>"><span class="fa fa-trash customer-icon-sm"></span></a>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
				</div>
			</div>
		</div>
	</div>
	<!-- <div class="col-md-1"></div> -->
</div>