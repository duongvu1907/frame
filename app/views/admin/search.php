<div class="row customer-bg customer-pad" style="margin-top: 20px">
	<div class="col-md-1" style="padding: 5px;">
		<label class="text-white "> By </label>
	</div>
	<div class="col-md-2" style="padding: 5px;">
		<select class="custom-select" id="filter" required="">
			<option value="books" selected="">Sách</option>
			<option value="authors">Tác giả</option>
			<option value="publishers">Nhà xuất bản</option>
			
		</select>
	</div>
	<script type="text/javascript">
		function setValue(btn){
			var txt = $(btn).text();
			$("#keyword").val(txt);
			$(".data-search").css('display', 'none');
		}
	</script>
	<div class="col-md-5" style="padding: 5px;">
		<input type="text" class="form-control" id="keyword" name="key" placeholder="Keyword **">
		<ul class="data-search">
			
		</ul>
	</div>
	<div class="col-md-2" style="padding: 5px;">
		<button class="btn btn-success">Search</button>
	</div>
	<div class="col-md-2"></div>
</div>
<div class="row customer-bg customer-pad" style="margin-top: 20px">
	<div class="col-md-2"></div>
	<div class="col-md-8">
		
		<h4>Thong tin chi tiet</h4>
		<div class="detail text-center text-white">
			
		</div>
	</div>
	<div class="col-md-2"></div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		
		$("#filter").change(function(event) {
			$("#keyword").val("");
			$(".data-search").css('display', 'none');
		});
		
		$("#keyword").keyup(function(event) {
			$(".data-search").css('display', 'block');
			$(".data-search").html("");
			var keyword = $("#keyword").val();
			var filter = $("#filter").val();
			$.ajax({
				url: "<?php echo url('admin/search/input') ?>",
				type: 'POST',
				dataType: 'json',
				data: {"filter": filter,"keyword":keyword},
				success:function(data){
					$.each(data, function(index, val) {
						 var li = "<li onclick='setValue(this)'>"+val.name+"</li>";
						 $(".data-search").append(li);
					});
				}
			})
		});
		
		
	});
</script>