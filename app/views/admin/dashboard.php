<?php use app\database\DB; ?>
<style type="text/css">
	h4{padding: 10px;}
</style>
<div class="row">
	<div class="col">
		<p class="text-white mt-5 mb-5">Welcome back, <b><?php echo $user->name ?></b></p>
	</div>
</div>
<!-- row -->
<div class="row tm-content-row">
	<div class="col-md-5 customer-bg">
		<table class="table table-hover">
			<h4 class="text-white">Đơn hàng cần xử lý</h4>
			<thead>
				<tr>
					<th>Mã ĐH</th>
					
					<th>Tên KH</th>
					<th>Trạng thái</th>
					<th></th>
				</tr>
				<tbody>
					<?php foreach ($orders_new as $order): ?>
						<?php $customer = DB::table("customers")->where("id","=",$order->customer_id)->first() ?>
					<tr>
						<td><?php echo $order->id ?></td>
						<td><?php echo $customer->username ?></td>
						<td><span class="fa fa-<?php echo $order->status!=0?'check text-success':'ban text-danger' ?>"></span></td>
						<td>	
							<a href="<?php echo url('orders/active/'.$order->id) ?>" class="text-success">kích hoạt ....</a>
						</td>
					</tr>
					<?php endforeach ?>
				</tbody>
			</thead>
		</table>
	</div>
	<div class="col-md-2"></div>
	<div class="col-md-5 customer-bg">
		<table class="table table-hover">
			<h4 class="text-white">Đơn hàng đã xử lý</h4>
			<thead>
				<tr>
					<th>Mã ĐH</th>
					
					<th>Tên KH</th>
					<th>Status</th>
					<th></th>
				</tr>
				<tbody>
					<?php foreach ($orders_old as $order): ?>

						<?php $customer = DB::table("customers")->where("id","=",$order->customer_id)->first() ?>
					<tr>
						<td><?php echo $order->id ?></td>
						<td><?php echo $customer->username ?></td>
						<td><span class="fa fa-check text-success"></span></td>
						<td>	
							<a href="<?php echo url('orders/active/'.$order->id) ?>" class="text-success">Xem</a>
						</td>
					</tr>
					<?php endforeach ?>
				</tbody>
			</thead>
		</table>
	</div>
</div>
<div class="row" style="margin-top: 50px">
	<div class="col-md-6 customer-bg">
		<h4 class="text-center text-white">Sách mua nhiều</h4>
		<table class="table-hover table">
			<tr>
				<th>#</th>
				<th>Ten sach</th>
				<th>So luong mua</th>
			</tr>
			<?php foreach ($books_sale_best as $book): ?>
				<tr>
					<td><?php echo $book->id ?></td>
					<td><?php echo $book->name?></td>
					<td><?php echo $book->orders_number ?></td>
				</tr>
			<?php endforeach ?>
		</table>
	</div>
	<div class="col-md-6 customer-bg">
		<h4 class="text-center text-white">Thể loại ưa thích</h4>
		<table class="table-hover table">
			<tr>
				<th>#</th>
				<th>Ten the loai</th>
			</tr>
			<?php foreach ($genres_best as $genre): ?>
				<tr>
					<td><?php echo $genre->id ?></td>
					<td><?php echo $genre->name?></td>
				</tr>
			<?php endforeach ?>
		</table>
	</div>
</div>
<div class="row customer-bg customer-pad" style="margin-top: 50px">
	<div class="col-md-2"></div>
	<div class="col-md-8">
		<h4 class="text-center text-white">Danh sách khách hàng</h4>
		<table class="table table-hover">
			<tr>
				<th>#</th>
				<th>Tên</th>
				<th>Ten truy cap</th>
				<th>Email</th>
				<th>Địa chỉ</th>
				<th>Số ĐT</th>
				<th></th>
			</tr>
			<?php foreach ($customers as $c): ?>
				<?php 
				$commune = DB::table("communes","uft8")->where("id","=",$c->commune_id)->first();
				$district = DB::table("districts","uft8")->where("id","=",$commune->district_id)->first();
				$province = DB::table("provinces","uft8")->where("id","=",$district->province_id)->first();
				$address = $commune->name." - ".$district->name." - ".$province->name;
				 ?>
				<tr>
					<td><?php echo $c->id ?></td>
					<td><?php echo $c->name ?></td>
					<td><?php echo $c->username ?></td>
					<td><?php echo $c->email ?></td>
					<td><?php echo $address ?></td>
					<td><?php echo $c->phone ?></td>
					<td>
						<a href="<?php echo url('admin/customer/d/'.$c->id) ?>" onclick="return confirm('Are you sure ?')"><span class="fa fa-trash text-danger"></span></a>
					</td>
				</tr>
			<?php endforeach ?>
		</table>
	</div>
	<div class="col-md-2"></div>
</div>