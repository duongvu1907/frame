<div class="row" style="margin-top: 50px">
	<div class="col-md-7 customer-bg customer-pad">
		<!-- Nav tabs -->
		<ul class="nav nav-tabs">
			<li class="nav-item">
				<a class="nav-link active" data-toggle="tab" href="#available">Sẵn có</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" data-toggle="tab" href="#expiried">Hết hạn</a>
			</li>
		</ul>

		<!-- Tab panes -->
		<div class="tab-content">
			<div class="tab-pane active container" id="available">
				<table class="table table-hover">
					<tr>
						<th>#</th>
						<th>Tên</th>
						<th>Giá trị</th>
						<th>Hết hạn</th>
						<th>Bắt đầu</th>
						<th></th>
					</tr>
					<?php foreach ($discounts_available as $discount): ?>
						<tr>
							<td class="discount_id"><?php echo $discount->id ?></td>
							<td><?php echo $discount->name ?></td>
							<td><?php echo $discount->value ?>%</td>
							<td><?php echo $discount->expiry_at ?></td>
							<td><?php echo $discount->start_at ?></td>
							<td>
								<span class="fa fa-edit customer-icon-sm edit"></span>&nbsp;
								<a href="<?php echo url('admin/discounts/d/'.$discount->id) ?>"><span class="fa fa-trash customer-icon-sm" ></span></a>
							</td>
						</tr>
					<?php endforeach ?>
				</table>
			</div>
			<div class="tab-pane container" id="expiried">
				<table class="table table-hover">
					<tr>
						<th>#</th>
						<th>Tên</th>
						<th>Giá trị</th>
						<th>Hết hạn</th>
						<th>Bắt đầu</th>
						<th></th>
					</tr>
					<?php foreach ($discounts_expiried as $discount): ?>
						<tr>
							<td class="discount_id"><?php echo $discount->id ?></td>
							<td><?php echo $discount->name ?></td>
							<td><?php echo $discount->value ?>%</td>
							<td><?php echo $discount->expiry_at ?></td>
							<td><?php echo $discount->start_at ?></td>
							<td>
								<span class="fa fa-edit customer-icon-sm edit"></span>&nbsp;
								<a href="<?php echo url('admin/discounts/d/'.$discount->id) ?>"><span class="fa fa-trash customer-icon-sm" ></span></a>
							</td>
						</tr>
					<?php endforeach ?>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-1" ></div>
	<div class="col-md-4 customer-bg customer-pad text-white" >
		<form action="<?php echo url('admin/discounts/add') ?>" method="post" enctype="multipart/form-data">
			<div class="form-group">
				<label>Tên</label>
				<input type="text" name="name" class="form-control">
			</div>
			<div class="form-group">
				<label>Giá trị</label>
				<input type="number" name="value" class="form-control">
			</div>
			<div class="form-group">
				<label>Bắt đầu</label>
				<input type="text" class="date-picker" name="start_at" class="form-control" autocomplete="off">
			</div>
			<div class="form-group">
				<label>Hết hạn</label>
				<input type="text" class="date-picker" name="expiry_at" class="form-control" autocomplete="off">
			</div>
			<div class="form-group">
				<!-- <label>Ảnh</label> -->
				<input type="file" name="image"  >
			</div>

			<div class="form-group">
				<button class="btn btn-primary" type="submit">Thêm </button>
			</div>

		</form>
	</div>
</div>
<div class="modal fade" id="edit-modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title">Edit</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-10">
						<form action="<?php echo url('admin/discounts/edit') ?>" method="post" enctype="multipart/form-data">
							<input type="number" hidden="" name="id" id="id">
							<div class="form-group">
								<label>Tên</label>
								<input type="text" name="name" class="form-control" id="name" style="background:#fff;color:#000;border:1px solid #ebebeb">
							</div>
							<div class="form-group">
								<label>Giá trị</label>
								<input type="number" name="value" class="form-control" id="value" style="background:#fff;color:#000;border:1px solid #ebebeb">
							</div>
							<div class="form-group">
								<label>Ngày bắt đầu</label>
								<input type="text" name="start_at" class="date-picker" style="background:#fff;color:#000;border:1px solid #ebebeb">
								<input type="text" disabled="" class="form-control" id="start_at" style="color:#000;">
							</div>
							<div class="form-group">
								<label>Ngày kết thúc</label>
								<input type="text" name="expiry_at" class="date-picker" style="background:#fff;color:#000;border:1px solid #ebebeb">
								<input type="text" disabled="" class="form-control" id="expiry_at" style="color:#000;">
							</div>
							<div class="form-group">
								<label>Ảnh</label>
								<input type="file" name="image" id="image" >
								<div>
									<img src="" id="discount-img" alt="" width="120px">
								</div>
							</div>
							<div class="form-group">
								<button class="btn btn-primary" type="submit">Sửa </button>
							</div>

						</form>
					</div>
					<div class="col-md-1"></div>
				</div>
			</div>

			<!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>

		</div>
	</div>
</div>
<script type="text/javascript">
	$(".date-picker").datetimepicker({
		format:"Y-m-d h:i:s"
	});
	$(".edit").click(function(event) {
		var id = parseInt($(this).parent().parent().find('.discount_id').text());
		$.ajax({
			url: "<?php echo url('admin/discounts/ajax') ?>",
			type: 'POST',
			dataType: 'json',
			data: {"id": id},
			success:function(data){
				$("#id").val(data.id);
				$("#name").val(data.name);
				$("#value").val(data.value);
				$("#expiry_at").val(data.expiry_at);
				$("#start_at").val(data.start_at);
				$("#discount-img").attr('src', "<?php echo assets('upload/discounts/') ?>"+data.image);
				$("#edit-modal").modal("show");
			}
		})
		
	});
</script>