<div class="row" style="margin-top: 50px">
	<div class="col-md-12 col-sm-12 customer-auto customer-pad">
		<div class="row">
			<div class="col-md-7 customer-bg" style="padding: 50px 20px;">
				<div class="table-container">
					<div class="alert text-danger">
						<?php if(isset($errors)){
							foreach ($errors as $e) {
								echo $e.", ";
							}
						} ?>
					</div>
					<table class="table table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Tên</th>
							<th>Nghệ danh</th>
							<th>Nghề nghiệp</th>
							<th>Địa chỉ</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($authors as $author): ?>
							<tr>
								<td class="author_id"><?php echo $author->id ?></td>
								<td><?php echo $author->name ?></td>
								<td><?php echo $author->stage ?></td>
								<td><?php echo $author->job ?></td>
								<td><?php echo $author->address ?></td>
								<td>
									<span class="fa fa-edit customer-icon-sm modal-edit"></span>
									&nbsp;&nbsp;
									<a href="<?php echo url('admin/authors/delete/'.$author->id) ?>"><span class="fa fa-trash customer-icon-sm"></span></a>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-4 customer-bg text-white" style="padding: 10px;">
				<form action="authors/add" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<label>Tên</label>
						<input type="text" name="name" class="form-control">
					</div>
					<div class="form-group">
						<label>Nghệ danh</label>
						<input type="text" name="stage" class="form-control">
					</div>
					<div class="form-group">
						<label>Nghề nghiệp</label>
						<input type="text" name="job" class="form-control">
					</div>
					<div class="form-group">
						<label>Địa chỉ</label>
						<input type="text" name="address" class="form-control">
					</div>
					<div class="form-group">
						<!-- <label>Ảnh</label> -->
						<input type="file" name="image" >
					</div>
					
					<div class="form-group">
						<button class="btn btn-primary" type="submit">Thêm </button>
					</div>
					
				</form>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="edit-modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
       	<div class="row">
       		<div class="col-md-1"></div>
       		<div class="col-md-10">
       			<form action="authors/edit" method="post" enctype="multipart/form-data">
       				<input type="number" hidden="" name="id" id="id">
					<div class="form-group">
						<label>Tên</label>
						<input type="text" name="name" class="form-control" id="name" style="background:#fff;color:#000;border:1px solid #ebebeb">
					</div>
					<div class="form-group">
						<label>Nghệ danh</label>
						<input type="text" name="stage" class="form-control" id="stage" style="background:#fff;color:#000;border:1px solid #ebebeb">
					</div>
					<div class="form-group">
						<label>Nghề nghiệp</label>
						<input type="text" name="job" class="form-control" id="job" style="background:#fff;color:#000;border:1px solid #ebebeb">
					</div>
					<div class="form-group">
						<label>Địa chỉ</label>
						<input type="text" name="address" class="form-control" id="address" style="background:#fff;color:#000;border:1px solid #ebebeb">
					</div>
					<div class="form-group">
						<label>image</label>
						<input type="file" name="image" id="image" >
						<div>
							<img src="" id="img-uth" alt="" width="120px">
						</div>
					</div>
					<div class="form-group">
						<button class="btn btn-primary" type="submit">Sửa </button>
					</div>
					
				</form>
       		</div>
       		<div class="col-md-1"></div>
       	</div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
<script type="text/javascript">
	$(".modal-edit").click(function(event) {
		
		var id = parseInt($(this).parent().parent().find('td.author_id').text());
		$.ajax({
			url: "<?php echo url('admin/authors/ajax') ?>",
			type: 'POST',
			dataType: 'json',
			data: {"id": id},
			success:function(data){
				$("#id").val(data.id);
				$("#name").val(data.name);
				$("#address").val(data.address);
				$("#job").val(data.job);
				$("#stage").val(data.stage);
				$("#edit-modal").modal("show");
				$("#img-uth").attr('src', "<?php echo assets('upload/authors/') ?>"+data.image);
			}
		})
		
		

	});
	function readURL(input) {

		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function(e) {
				jQuery('#img-uth').attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
		}
	}

	$("#image").change(function() {
		readURL(this);
	});
</script>