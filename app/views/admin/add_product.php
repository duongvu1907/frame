<div class="row" >
	<div class="col-xl-9 col-lg-10 col-md-12 col-sm-12 customer-auto">
		<div class="customer-pad customer-bg">
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<h4 class="text-center text-white">Add Product</h4>
				</div>
				<div class="col-md-2"></div>
			</div>
			<form action="<?php echo url('admin/product/add') ?>" method="post"  enctype="multipart/form-data">
				<div class="row text-white">
					<div class="col-xl-6 col-lg-6 col-md-12">
						<div class="form-group">
							<label>Tên sản phẩm</label>
							<input type="text" name="name" required="" placeholder="Tên sản phẩm "  class="form-control">
						</div>

						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label>Thể loại</label>
									<select name="genres_id[]" class=" chzn-select form-control" data-placeholder="Chọn thể loại" required="" multiple  style="background:#54657d!important;color:#fff" >
										<?php foreach ($genres as $genre): ?>
											<option value="<?php echo $genre->id ?>"><?php echo $genre->name ?></option>
										<?php endforeach ?>
									
									</select>
									<!-- <button class="btn-cs">thêm thể loại</button> -->
								</div>

							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label>Tác giả</label>
									<select class="custom-select" required="" name="author_id">
										<option value="">Chọn tác giả</option>
										<?php foreach ($authors as $author): ?>
											<option value="<?php echo $author->id ?>"><?php echo $author->name ?></option>
										<?php endforeach ?>
									</select>
									
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label>Nhà xuất bản</label>
									
									<select class="custom-select" required="" name="publisher_id">
										<option value="" >Chọn nhà sx</option>
										<?php foreach ($publishers as $publisher): ?>
											<option value="<?php echo $publisher->id ?>"><?php echo $publisher->name ?></option>
										<?php endforeach ?>
									</select>
									
									<!-- <button class="btn-cs">thêm thể loại</button> -->
								</div>

							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label>Số lượng</label>
									<input type="number" placeholder="Số lượng" name="quantity" class="form-control">
									
									<!-- <button class="btn-cs">thêm thể loại</button> -->
								</div>

							</div>
						</div>
						<div class="form-group">
									<label>Giá bán</label>
									<input type="number" placeholder="VND" name="price" class="form-control">
									
									<!-- <button class="btn-cs">thêm thể loại</button> -->
								</div>
						
					</div>
					<div class="col-xl-6 col-lg-6 col-md-12">
						
						<!-- <div class="file-upload">
							<i class="fas fa-cloud-upload-alt customer-icon" onclick="document.getElementById('fileInput').click();"></i>
						</div> -->
						<div class="custom-file customer-auto text-center" style="margin-top: 20px!important;">
							<input type="file" name="image" required="" id="fileInput" style="display: none;">
							<span class="btn btn-primary " onclick="document.getElementById('fileInput').click();" style="width: 100%">TẢI ẢNH LÊN</span>
						</div>
						<div class="image-upload" style="width: 100%;text-align: center;margin-top: 20px">
							<img src="" id="output-img" alt="" width="200px">
						</div>
					</div>
					
					<div style="width:100%;margin:auto;text-align: center">
						<div class="col-12">
							<div class="row">
						<div class="col-12">
							<div class="form-group">
							<label>Mô tả</label>
							<textarea class="form-control description-data" name="description">

							</textarea>
							<script>tinymce.init({selector:'.description-data'})</script>
						</div>
						</div>
					</div>
							<button class="btn btn-primary text-uppercase" type="submit" style="width: 100%;text-align: center;" >Thêm sản phẩm</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(".chzn-select").chosen({

	});
	function readURL(input) {

		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function(e) {
				jQuery('#output-img').attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
		}
	}

	$("#fileInput").change(function() {
		readURL(this);
	});
</script>