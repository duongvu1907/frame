<?php use app\database\DB; ?>
<div class="row" style="margin-top: 50px">
	<div class="col-md-7 customer-bg customer-pad">
		<div class="card">
			<div class="card-header">
				<h4>Slider</h4>
			</div>
			<div class="card-body">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Ảnh</th>
							<th>Tên SP</th>
							<th>Tiêu đề</th>
							<th>Description</th>
							<th></th>
						</tr>
					</thead>
					<tbody>

						<?php foreach ($sliders as $slider): ?>
							<?php $data = DB::table($slider->filter)->where("id","=",$slider->foreign_key)->first() ?>
							<tr>
								<td class="slider_id"><?php echo $slider->id ?></td>
								<td><?php echo $data->name ?></td>
								<td><img src="<?php echo assets('upload/sliders/'.$slider->image) ?>" alt="" width="120px"></td>
								<td><?php echo $slider->title;?></td>
								<td><?php echo $slider->description;?></td>
								<td>
									<span class="fa fa-edit edit-slider customer-icon-sm"></span> &nbsp;
									<a href="<?php echo url('admin/customize/slider/d/'.$slider->id) ?>"><span class="fa fa-trash customer-icon-sm"></span></a>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-5 text-white customer-pad customer-bg">
		<form action="<?php echo url('admin/customize/slider/add') ?>" method="post" enctype="multipart/form-data">
					<div class="form-group" style="position: relative;">
						<input type="number" name="foreign_key" id="foreign_key" hidden="">
						<select class="custom-select" name="filter" id="filter">
							<option value="books">Sách</option>
							<option value="genres">Thể loại</option>
							<option value="authors">Tác giả</option>
							<option value="publishers">Nhà xuất bản</option>
						</select>

						<label>Tên sản phẩm</label>
						<input type="text" name="name" class="form-control" autocomplete="off" id="keyword">
						<ul class="data-search" style="top:115px;">
							
						</ul>
						<script type="text/javascript">
							function setValue(btn,id){
								var txt = $(btn).text();
								$("#keyword").val(txt);
								$("#foreign_key").val(id);
								$(".data-search").html('');
								$(".data-search").css('display', 'none');
							}
						</script>
					</div>
					<div class="form-group">
						<label>Tiêu đề</label>
						<input type="text" name="title" class="form-control" required="">
					</div>
					<div class="form-group">
						<label>Mô tả</label>
						<textarea class="form-control" name="description" required=""></textarea>
					</div>
					<div class="form-group">
						<input type="file" name="image" required="" id="file_input" required="">
					</div>
					<div class="form-group">
						<button class="btn btn-primary" type="submit">Thêm </button>
					</div>
				</form>
	</div>
</div>
<div class="modal fade" id="edit-modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
<div class="modal-body">
       	<div class="row">
       		<div class="col-md-1"></div>
       		<div class="col-md-10">
       			<form action="<?php echo url('admin/customize/slider/edit') ?>" method="post" enctype="multipart/form-data">
       				<input type="number" hidden="" name="id" id="id">
					<div class="form-group">
						<label>Tên</label>
						<input type="text" name="name" class="form-control" disabled="" id="name" style="background:#fff;color:#000;border:1px solid #ebebeb">
					</div>
					<div class="form-group">
						<label>Tiêu đề</label>
						<input type="text" name="title" class="form-control" id="title" style="background:#fff;color:#000;border:1px solid #ebebeb">
					</div>
					<div class="form-group">
						<label>Mô tả</label>
						<textarea name="description" class="form-control" id="description" style="background:#fff;color:#000;border:1px solid #ebebeb"></textarea>
					</div>
					<div class="form-group">
						<label>Ảnh</label>
						<input type="file" name="image" id="image" >
						<div>
							<img src="" id="slider-img" alt="" width="120px">
						</div>
					</div>
					<div class="form-group">
						<button class="btn btn-primary" type="submit">Sửa </button>
					</div>
					
				</form>
       		</div>
       		<div class="col-md-1"></div>
       	</div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
<script type="text/javascript">
	
	jQuery(document).ready(function($) {
		$("#keyword").keyup(function(event) {
			$(".data-search").css('display', 'block');
			$(".data-search").html("");
			var keyword = $("#keyword").val();
			var filter = $("#filter").val();
			$.ajax({
				url: "<?php echo url('admin/search/input') ?>",
				type: 'POST',
				dataType: 'json',
				data: {"filter": filter,"keyword":keyword},
				success:function(data){
					$.each(data, function(index, val) {
						 var li = "<li style='color:#000' onclick='setValue(this,"+val.id+")'>"+val.name+"</li>";
						 $(".data-search").append(li);
					});
				}
			})
		});
		$(".edit-slider").click(function(event) {
			var slider_id = parseInt($(this).parent().parent().find('.slider_id').text());
			$.ajax({
				url: "<?php echo url('admin/customize/slider/ajax') ?>",
				type: 'POST',
				dataType: 'json',
				data: {"slider_id": slider_id},
				success:function(data){
					$("#id").val(data.slider.id);
					$("#name").val(data.filter.name);
					$("#description").val(data.slider.description);
					$("#title").val(data.slider.title);
					var image = data.slider.image;
					
						image = "<?php echo assets("") ?>"+"upload/sliders/"+image;
					
					$("#slider-img").attr('src', image);
					$("#edit-modal").modal("show");
				}
			})
			
		});
	});
</script>

<div class="row customer-bg customer-pad text-white" style="margin-top: 50px">
	<div class="col-7">
		<div class="card">
			<div class="card-header">
				<h4>Sản phẩm đặc biệt</h4>
			</div>
			<div class="card-body">
				<table class="table table-hover">
					<tr>
						<th>#</th>
						<th>Tên sản phẩm</th>
						<th>Ảnh</th>
						<th>Tiều đề</th>
						<th></th>
					</tr>
					<?php foreach ($features_books as $fb): ?>
						<?php $book = DB::table("books")->select(["name"])->where("id","=",$fb->book_id)->first() ?>
						<tr>
							<td><?php echo $fb->id ?></td>
							<td><?php echo $book->name ?></td>
							<td><img src="<?php echo assets('upload/features_books/'.$fb->image) ?>" width="120px"></td>
							<td><?php echo $fb->title ?></td>
							<td>
								&nbsp;
									<a href="<?php echo url('admin/customize/features-books/d/'.$fb->id) ?>"><span class="fa fa-trash customer-icon-sm"></span></a>
							</td>
						</tr>
					<?php endforeach ?>
				</table>
			</div>
		</div>
	</div>
	<div class="col-5">
		<form action="<?php echo url('admin/customize/features-books/add') ?>" method='post' enctype="multipart/form-data">
			<input type="number" id="book_id" name="book_id" hidden="">
			<div class="form-group">
				<label>Ten sach</label>
				<input type="text" class="form-control" id="keyword_books" required="" autocomplete="off">
				<ul class="data-search" id="once" style="top:75px;">
					
				</ul>
				<script type="text/javascript">
							function setValue_2(btn,id){
								var txt = $(btn).text();
								$("#keyword_books").val(txt);
								$("#book_id").val(id);
								$("#once").html('');
								$("#once").css('display', 'none');
							}
						</script>
			</div>
			<div class="form-group">
				<label>Tieu de</label>
				<input type="text" class="form-control" name="title" id="book_title" required="">
			</div>
			<div class="form-group">
				
				<input type="file" name="image" id="keyword_books" required="">
			</div>
			<div class="form-group">
				
				<button class=" btn btn-primary">Thêm</button>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	$("#keyword_books").keyup(function(event) {
			
			$(this).parent().find(".data-search").html("");
			var keyword = $(this).val();
			var filter = "books";
			$.ajax({
				url: "<?php echo url('admin/search/input') ?>",
				type: 'POST',
				dataType: 'json',
				data: {"filter": filter,"keyword":keyword},
				success:function(data){
					$("#once").css('display', 'block');
					$.each(data, function(index, val) {
						 var li = "<li style='color:#000' onclick='setValue_2(this,"+val.id+")'>"+val.name+"</li>";
						 console.log(li);
						 $("#once").append(li);
					});
				}
			})
		})
</script>
