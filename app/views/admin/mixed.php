<script type="text/javascript">
	function setValue(btn,id){
		var txt = $(btn).text();
		$(btn).parent().parent().find('input.key').val(txt);
		console.log(id);
		$(btn).parent().parent().find("input.data-form").val(id);
		$(btn).parent().css('display', 'none');
	}
</script>
<div class="row">
	<div class="col-md-12">
		<div class="row text-white" style="margin:50px 10px">
			<!-- <div class="col-md-2"></div> -->
			<div class="col-md-12 customer-bg customer-pad">
				<h4 class="text-center">Giảm giá cho sách</h4>
				<form action="<?php echo url('admin/discounts-books/add') ?>" method="post">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Tên sách</label>
								<input type="number" name="book_id" hidden="" class="data-form">
								<input type="text" class="form-control key" name="name" id="book_name" autocomplete="off">
								<ul class="data-search data-search-book" style="top:80px;color:#000">

								</ul>
							</div>

						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Giảm giá</label>
								<input type="number" name="discount_id" hidden="" class="data-form">
								<input type="text" class="form-control discount_name key" name="name" autocomplete="off">
								<ul class="data-search" style="top:80px;color:#000">
								</ul>
							</div>

						</div>
						
					</div>
					<button class="btn btn-primary">Tạo</button>
				</form>
			</div>
			<!-- <div class="col-md-2"></div> -->
		</div>
	</div>
	<!-- <div class="col-md-6">
		<div class="row text-white" style="margin:50px 10px">
			
			<div class="col-md-12 customer-bg customer-pad">
				<h4 class="text-center">Giảm giá cho khách</h4>
				<form action="<?php  ?>" method="post">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Tên khách hàng </label>
								<input type="number" name="customer_id" hidden="" class="data-form">
								<input type="text" class="form-control key" name="name" id="customer_name"  autocomplete="off">
								<ul class="data-search" style="top:80px;color:#000">
								</ul>
							</div>

						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Giảm giá</label>
								<input type="number" name="discount_id" hidden="" class="data-form">
								<input type="text" class="form-control discount_name key" name="name" class="" autocomplete="off" >
								<ul class="data-search" style="top:80px;color:#000">
								</ul>
							</div>

						</div>
				
					</div>
					<button class="btn btn-primary">Tạo</button>
				</form>
			</div>
		
		</div>
	</div> -->
</div>



<div class="row text-white" style="margin:50px 10px;">
	<div class="col-md-2"></div>
	<div class="col-md-8 customer-bg customer-pad">
		<h4 class="text-center">Lọc</h4>
		<div class="row">
			<div class="col-md-4">
				<select class="custom-select" name="filter" id="filter">
					<option value="books">Sách</option>
					<option value="customers">Khách hàng</option>
					<!-- <option value=""></option> -->
				</select>
			</div>
			<div class="col-md-4">
				<input type="number" class="data-form" hidden="" id="id" id="">
				<input type="text" name="keyword" id="find" class="form-control key">
				<ul class="data-search data-search-one" style="color:#000"></ul>
			</div>
			<div class="col-md-4">
				<button class="btn btn-primary ajax">Xem</button>
			</div>
		</div>
		<script>
			
		</script>
	</div>
	<div class="col-md-2"></div>
</div>
<div class="row text-white" style="margin:50px 10px;">
	<div class="col-md-2"></div>
	<div class="col-md-8 customer-bg customer-pad">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>#</th>
					<th>KH / Sách</th>
					<th>Tên</th>
					<th>Giá trị</th>
					<th>Trang thái</th>
					<th></th>
				</tr>
			</thead>
			<tbody class="data-discounts">
				
			</tbody>
		</table>
	</div>
	<div class="col-md-2"></div>
</div>
<script type="text/javascript">
	$("#book_name").keyup(function(event) {
		started(this);
		var keyword = $(this).val();
		var url = "<?php echo url('admin/product/ajax') ?>";
		requestHttp(url,keyword,this);
		
	});
	$("#customer_name").keyup(function(event) {
		started(this);
		var keyword = $(this).val();
		var url = "<?php echo url('admin/customer/ajax') ?>";
		requestHttp(url,keyword,this);
		
	});
	$(".discount_name").keyup(function(event) {
		started(this);
		var keyword = $(this).val();
		var url = "<?php echo url('admin/discounts/ajax_name') ?>";
		requestHttp(url,keyword,this);
	});
	$("#filter").change(function(event) {
		$("#id").val("");$("#find").val("");$(".data-search-one").css('display', 'none');
	});
	$("#find").keyup(function(event) {
				$(".data-search-one").css('display', 'block');
				var filter = $("#filter").val();
				var url = "";
				if (filter == "books") {
					url = "<?php echo url('admin/product/ajax') ?>";
				}else{
					url = "<?php echo url('admin/customer/ajax') ?>";
				}
				var keyword = $(this).val();
				requestHttp(url,keyword,this);
			});
	function requestHttp(url,keyword,self){
		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'json',
			data: {"keyword": keyword},
			success:function(data){
				finished(self);
				$.each(data, function(index, val) {
					var li = "<li onclick='setValue(this,"+val.id+")'>"+val.name+"</li>";
					$(self).parent().find('.data-search').append(li);
				});
			}
		})
		
	}
	function started(self){
		$(self).parent().find('.data-search').css('display', 'block');
	}
	function finished(self){
		$(self).parent().find('.data-search').html("");
	}
	$(".ajax").click(function(event) {
		var filter = $("#filter").val();
		var id = $("#id").val();
		$.ajax({
			url: "<?php echo url('admin/discounts/statistics') ?>",
			type: 'POST',
			dataType: 'json',
			data: {"id": id,"filter":filter},
			success:function(data){
				var url = "<?php echo url('admin/discounts/mixed/d') ?>";
				$(".data-discounts").html("");
				$.each(data, function(index, val) {
					var status = "";
					if (val.status == 1) {
						status = "<span class='fa fa-check text-success'></span>";
					}else{
						status = "<span class='fa fa-ban text-danger'></span>";
					}
					 var tr = "<tr>"+"<td>"+val.id+"</td>"+
					 "<td>"+val.name+"</td>"+
					 "<td>"+val.discount_name+"</td>"+
					 "<td>"+val.discount_value+"</td>"+
					 "<td>"+status+"</td>"+
					 "<td>"+"<a href='"+url+"/"+val.filter+"/"+val.id+"'><span class='fa fa-trash customer-icon-sm'></span></a>"+"</td>"+
					 "</tr>";
					 $(".data-discounts").append(tr);
				});
			}
		})
	});
</script>

