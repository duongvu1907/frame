<?php use app\database\DB; ?>
<div style="width: 500px;background: lavender">
	<h4 style="text-align: center;padding:20px;color:green;text-transform: uppercase;">Đơn hàng mã số : <?php echo $order_id ?></h4>
	<div class="car-body" style="background: #fff;padding: 10px;">
		<ul style="padding: 20px;">
			<?php $total = 0; ?>
			<?php foreach ($details as $d): ?>
				<?php $total+=$d->quantity*$d->price; ?>
				<?php $book = DB::table("books")->where("id","=",$d->book_id)->first() ?>	
				<li style="padding: 5px"> Tên sách : <?php echo $book->name ?> - <?php echo number_format($d->price) ?><sup>đ</sup> x <?php echo $d->quantity ?></li>
			<?php endforeach ?>
			<li style="padding: 5px">Tổng đơn hàng : <?php echo number_format($total); ?> <sup>đ</sup></li>
		</ul>
		<p class="text-success" style="padding: 10px 20px;">Đang xử lý</p>
		<hr>
		<ul style="padding: 20px">
			<li style="padding: 5px">Tên khách hàng : <?php echo $user->name ?></li>
			<li style="padding: 5px">Địa chỉ  : <?php echo $address ?></li>
			<li style="padding: 5px">Số điện thoại  : <?php echo $user->phone ?></li>
		</ul>
	</div>
</div>