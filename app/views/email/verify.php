
	<div class="wrapper" >
		<div class="card" style="width: 600px;margin:auto;padding: 10px">
			<div class="card-header" style="padding: 5px;text-align: center;text-transform: uppercase;color: #000;background: lavender">
				<h4 style="margin-bottom: 5px">Reset Password</h4>
				<img src="https://i.imgur.com/qGissYy.png" width="100px" alt="">
			</div>
			<div class="card-body" style="background: #0d8462;">
				<div class="row" style="text-align: center;">
					<div class="col-md-2"></div>
					<div class="col-md-8">
						<span class="text-success token" style="margin-top: 20px;padding: 8px;background: #e89008;text-align: center;font-size: 17px;color: #fff"><?php echo $token ?></span>
					</div>
					<div class="col-md-2"></div>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-8">
						<p>--------------------------</p>
						<ul>
							<li style="color: #fff">Information of Email : <?php echo $subject ?></li>
							<li style="color: #fff">Send From to : <?php echo $fromEmail ?></li>
							<li style="color: #fff">Send From to : <?php echo $info ?></li>
						</ul>
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
		</div>
	</div>
