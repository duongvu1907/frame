<div class="register" style="height: 300px">
	<div class="row">
		<div class="col-md-12">
			<div class="login-header">
				<h4 class="text-center">
					<img src="<?php echo assets('assets/admin/img/logo.png') ?>" alt="" width="100px">
				</h4>
			</div>
			<div class="login-body">
				<form action="<?php echo url('admin/login/verify/'.$id) ?>" method="post">
					<div class="form-group">
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-7">
								<span class="fa fa-signature"></span>
								<input type="text" name="token" placeholder="Token ***" required="" class="form-customer">
								<!-- <label for="name">Email</label> -->
								<input type="number" name="id" value="<?php echo $id ?>" hidden="">
							</div>
							<div class="col-md-3 text-center">
								<button class="btn btn-success">Xác thực</button>
							</div>
							<div class="col-md-1"></div>
						</div>
					</div>
				</form>
				<div class="error text-center">
					<?php 
						if (isset($_GET["ms_0"])) {
							foreach ($_GET as $value) {
								echo "<p class='text-danger bg-primary'>".$value."</p>";
							}
						}
					 ?>
				</div>
			</div>
		</div>
	</div>
</div>