<div class="register">
	<div class="row">
		<div class="col-md-12">
			<div class="login-header">
				<h4 class="text-center">
					<img src="<?php echo assets('assets/admin/img/logo.png') ?>" alt="" width="100px">
				</h4>
			</div>
			<div class="login-body">
				<form action="" method="post">
					<div class="form-group">
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-5">
								<span class="fa fa-signature"></span>
								<!-- <label for="name">Email</label> -->
								<input type="text" name="name" placeholder="Tên của bạn ***"  class="form-customer">
							</div>
							<div class="col-md-5">
								<span class="fa fa-user"></span>
								<!-- <label for="name">Email</label> -->
								<input type="text" name="email" placeholder=" Email ***"  class="form-customer">
							</div>
							<div class="col-md-1"></div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10">
								<span class="fa fa-lock"></span>
								<!-- <label for="password">Password</label> -->
								<input type="password" name="password" placeholder="Mật khẩu *** " id="password" class="form-customer">
							</div>
							<div class="col-md-1"></div>
						</div>
					</div>
					<div class="form-group">
					
						<div class="row" style="margin-top: 20px">
							<div class="col-md-1"></div>
							<div class="col-md-10">

								<button class="btn btn-success" type="submit">Đăng ký</button>
							</div>
							<div class="col-md-1"></div>
						</div>
						<div class="row" style="margin-top: 20px">
							<div class="col-md-1"></div>
							<div class="col-md-10">
								<a href="<?php echo url('admin/login/forget-password') ?>" class="link">Quên mật khẩu?</a>
							</div>
							<div class="col-md-1"></div>
						</div>
					</div>
				</form>
				<div class="error text-center">
					<?php 
						if (isset($_GET["ms_0"])) {
							foreach ($_GET as $value) {
								echo "<p class='text-danger bg-primary'>".$value."</p>";
							}
						}
					 ?>
				</div>
			</div>
		</div>
	</div>
</div>