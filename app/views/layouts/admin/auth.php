<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="https://www.favicon.cc/logo3d/554683.png" type="image/x-icon" />
	<title>Xác thực người dùng</title>
	<link rel="stylesheet" type="text/css" href="<?php echo assets("assets/admin/css/bootstrap.min.css") ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo assets("assets/admin/css/fontawesome.min.css") ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo assets("assets/admin/css/auth.css") ?>">

</head>
<body>
	<section>
		<div class="moving-clouds" style="background-image:url(<?php echo assets('assets/admin/img/clouds.png') ?>)"></div>	
		<?php echo $content ?>
		<div class="menu">
			<ul>
				<li><a href="<?php echo url('admin/login') ?>">Đăng nhập</a></li>
				<li><a href="<?php echo url('admin/register') ?>">Đăng ký</a></li>
			</ul>
		</div>
	</section>
</body>
</html>