<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Trang quản lý - Sách ABC</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,700">
    <link rel="stylesheet" href="<?php echo assets('assets/admin/css/fontawesome.min.css') ?>">
    <link rel="stylesheet" href="<?php echo assets('assets/admin/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.min.css">
    <link rel="stylesheet" href="<?php echo assets('assets/admin/css/style.min.css') ?>">
    <link rel="stylesheet" href="<?php echo assets('assets/admin/css/jquery.datetimepicker.min.css') ?>">

    <script src="<?php echo assets('assets/admin/js/jquery-3.3.1.min.js') ?>"></script>
    <!-- https://jquery.com/download/ -->
    <script src="<?php echo assets('assets/admin/js/bootstrap.min.js') ?>"></script>
    <script src="<?php echo assets('assets/admin/js/jquery.datetimepicker.full.min.js') ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js"></script>
    <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=1h1ckafkkqrcwdblbv9gbfeunus1fxmewt2sr1dezb0walx5"></script>

</head>
<body id="reportsPage">
    <div class="" id="home">
        <nav class="navbar navbar-expand-xl">
            <div class="container h-100">
                <a class="navbar-brand" href="<?php echo url('admin') ?>">
                    <h1 class="tm-site-title mb-0">
                       
                            <img src="<?php echo assets('logo.png') ?>" width="140px" alt="">
                       
                    </h1>
                </a>
                <button class="navbar-toggler ml-auto mr-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-bars tm-nav-icon"></i>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mx-auto h-100">
                        <li class="nav-item">
                            <a class="nav-link active" href="<?php echo url('admin/dashboard') ?>">
                                <i class="fas fa-tachometer-alt"></i>
                                Bảng điều khiển
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item dropdown">

                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <i class="far fa-file-alt"></i>
                                <span>
                                    Giảm giá <i class="fas fa-angle-down"></i>
                                </span>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="<?php echo url('admin/discounts/') ?>">Sẵn có</a>
                                <a class="dropdown-item" href="<?php echo url('admin/discounts/mixed') ?>">Ghép</a>
                                <!-- <a class="dropdown-item" href="#">Yearly Report</a> -->
                            </div>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo url('admin/product') ?>">
                                <i class="fas fa-shopping-cart"></i>
                                Sản phẩm
                            </a>
                        </li>
                         <li class="nav-item dropdown">

                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-pen"></i>
                                <span>
                                    Khác <i class="fas fa-angle-down"></i>
                                </span>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="<?php echo url('admin/authors') ?>">Tác giả</a>
                                <a class="dropdown-item" href="<?php echo url('admin/genres') ?>">Thể loại</a>
                                <a class="dropdown-item" href="<?php echo url('admin/publishers') ?>">Nhà xuất bản</a>
                                <a class="dropdown-item" href="<?php echo url('admin/search') ?>">Tìm kiếm sản phẩm</a>
                                
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-cog"></i>
                                <span>
                                    Cài đặt <i class="fas fa-angle-down"></i>
                                </span>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="<?php echo url('admin/profile') ?>">Hồ sơ</a>
                                <a class="dropdown-item" href="<?php echo url('admin/register') ?>">Đăng ký</a>
                                <a class="dropdown-item" href="<?php echo url('admin/customize') ?>">Tùy chỉnh</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo url('admin/logout') ?>">
                                <span class="fa fa-sign-out-alt"></span>
                                Đăng xuất
                            </a>
                        </li>
                    </ul>

                </div>
            </div>

        </nav>
        <div class="container">
            <?php echo $content; ?>
        </div>
        
    </div>

    
</body>
<script type="text/javascript">
</script>
</html>