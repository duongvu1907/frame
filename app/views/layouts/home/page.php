<?php use app\database\DB; ?>
<!DOCTYPE html>
<html>
<head>
	<title>CỬA HÀNG SÁCH ABC-  Lorem ipsum dolor sit amet</title>
	<link rel="stylesheet" type="text/css" href="<?php echo assets('assets/home/css/layout.min.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo assets('assets/home/css/style.min.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo assets('assets/home/css/responsive.min.css') ?>">
	<link href="http://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
	<link rel="stylesheet" type="text/css" href="<?php echo assets('assets/home/library/assets/owl.carousel.min.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo assets('assets/home/library/assets/owl.theme.default.min.css') ?>">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.css">
	<style type="text/css">
		.chosen-container-single .chosen-single{
			height: 36px;
			line-height: 34px;
		}
	</style>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js"></script>
</head>
	<body>
	<?php $genres = DB::table("genres")->get();
			$authors = DB::table("authors")->orderBy("id","DESC")->limit(10)->get();
			$publishers = DB::table("publishers")->get();
			 $books = DB::table("")->query(
			"SELECT books.id,books.author_id,books.image,books.name,books.price,count(book_id) as orders_number FROM books inner join orders_details ON orders_details.book_id = books.id group by books.id order by orders_number DESC LIMIT 10"
		); 
	 ?>
	<header class="nav">
		<div class="second-header">
			<div class="st-trigger">
				<i class="fa fa-bars"></i>
			</div>
			<div class="logo-header">
				<a href="<?php echo url('') ?>"><img src="<?php echo assets('logo.png') ?>" alt="" width="100%"></a>
			</div>
			<div class="container">
				<div class="menu">
					<div class="box-menu">
						<p>Danh mục &nbsp;&nbsp;<i class="fa fa-bars"></i></p>

					</div>
					<div class="momo"></div>
					<div class="list-category">
						<ul>
							<li class="dropdown">Sách theo thể loại
								<ul class="sub-menu">
									<?php foreach ($genres as $genre): ?>
										<li><a href="<?php echo url('the-loai/'.name_to_slug($genre->name,$genre->id)) ?>"><?php echo $genre->name ?></a></li>
									<?php endforeach ?>
								</ul>

							</li>
							<li class="dropdown">Sách theo tác giả
								<ul class="sub-menu">
									<?php foreach ($authors as $author): ?>
										<li><a href="<?php echo url('tac-gia/'.name_to_slug($author->name,$author->id)) ?>"><?php echo $author->name ?></a></li>
									<?php endforeach ?>
								</ul>
							</li>
							<li class="dropdown">Nhà xuất bản
								<ul class="sub-menu">
									<?php foreach ($publishers as $publisher): ?>
										<li><a href="<?php echo url('nha-xuat-ban/'.name_to_slug($publisher->name,$publisher->id)) ?>"><?php echo $publisher->name ?></a></li>
									<?php endforeach ?>
								</ul>
							</li>
						</ul>
						<div class="deal-hot text-center"><a href="<?php echo url('sach-moi-nhat') ?>">Sản phẩm mới &nbsp;&nbsp;<i class="fa fa-fire"></i></a> </div>
					</div>
				</div>
				<div class="box-input">
				<form id="search-form" action="" method="GET">
					<div class="search-box">
						
							<label>
								<select name="" id="genre-choose">
									<option value="" selected>Tất cả </option>
									<?php foreach ($genres as $genre): ?>
										<option value="<?php echo $genre->id ?>"><?php echo $genre->name ?></option>
									<?php endforeach ?>
								</select>
							</label>
							<label class="seach-input-box">
								<input type="text" placeholder="Tìm kiếm ***" id="search-books" autocomplete="off">
								
							</label>
							
							<button type="button" id="btn-search"><i class="fa fa-search"></i></button>
							
							<div class="options">
									<ul class="data-search" style="display: none;">
										
									</ul>
								</div>
					</div>
</form>
				</div>
			</div>

			<div class="account">
				<!-- <div class="right-account wish-list">
					<i class="fa fa-heart"></i> &nbsp;Wish List
				</div> -->
				
				<div class="cart">
					<i class="fa fa-shopping-cart"></i>
					<span class="number-product"><?php echo isset($_SESSION["cart"])?count($_SESSION["cart"]):0 ?></span>
					<div class="body-cart">
						<div class="cart-header">
							<?php $total = 0;if(isset($_SESSION["cart"])){ ?>
						<?php foreach ($_SESSION["cart"] as $cart): ?>
							<?php $total+= $cart["quantity"]*$cart["price_new"] ?>
							<div class="product-widget">
								<div class="product-img">
									<img src="<?php echo assets('upload/books/'.$cart["image"]) ?>" alt="">
								</div>
								<div class="product-body">
									<h3 class="product-name"><a href="<?php echo url('chi-tiet/').name_to_slug($cart['name'],$cart['id']) ?>"><?php echo $cart["name"] ?></a></h3>
									<h4 class="product-price"><span class="qty"><?php echo $cart["quantity"] ?>x</span><?php echo number_format($cart["price_new"]) ?><sup>đ</sup></h4>
								</div>
								<a href="<?php echo url('cart/delete/'.$cart["id"]) ?>">
								<button class="delete" ><i class="fa fa-close"></i></button>
								</a>
							</div>

						<?php endforeach ?>
					<?php } ?>

						</div>
						<div class="cart-summary">
							<small><?php echo isset($_SESSION["cart"])?count($_SESSION['cart']):0 ?> sản phẩm được chọn</small>
							<h5>Tổng giỏ hàng : <?php echo isset($_SESSION["cart"])?number_format($total):0 ?> <sup>đ</sup></h5>
						</div>
						<div class="cart-btns">
							<a class="close-c" style="cursor: pointer;">close</a>
							<a href="<?php echo url('u/gio-hang') ?>">Thanh toán  <i class="fa fa-arrow-circle-right"></i></a>
						</div>
						
					</div>
				</div>
				<div class="right-account sign" style="margin-left: 40px">
					<?php if(isset($_SESSION["username"])){ ?>
					<a href="<?php echo url('u/ho-so') ?>" class="text-white"><i class="fa fa-user"></i> &nbsp;<?php echo $_SESSION["username"] ?></a>
					<div class="act-user">
						<a href="<?php echo url('u/ho-so') ?>">Hồ sơ</a>
						<a href="<?php echo url('u/logout') ?>">Đăng xuất</a>
					</div>
				<?php }else{ ?>
					<a href="<?php echo url('dang-nhap') ?>" class="text-white"><i class="fa fa-user"></i> &nbsp;Đăng nhập</a>
				<?php } ?>
				</div>
			</div>
		</div>
	</header>
	<div style="clear: both;"></div>
	<div class="honor-keyword">
		<div class="container">
			<div class="label">
				<span class="h1-label text-left">Sách đang bán chạy :</span>
				<marquee onMouseOver="this.stop()" onMouseOut="this.start()" style="margin-top: 4px;">
					<ul>
						<?php foreach ($books as $book): ?>
							
						<li><a href="<?php echo url('chi-tiet/').name_to_slug($book->name,$book->id) ?>"><?php echo $book->name ?></a></li>

						<?php endforeach ?>
						
					</ul>
				</marquee>
				<span class="h1-label text-right">...</span>

			</div>
		</div>
	</div>
	<div class="clear"></div>
	<div class="body">
		<div class="container">
			
			<?php echo $content ?>
		</div>
	</div>
	<div class="footer" style="background: #262726;color: #fff;padding:15px;">
		<div class="body">
			
		<div class="container">
			<div class="row">
				<div class="col-7">
					<p style="font-size: 19px">© 2019 eMartica - Power By Me <i class="fa fa-signature"></i></p>
				</div>
				<div class="col-5 text-center">
					<img src="<?php echo assets('logo.png') ?>" alt="" style="filter: grayscale(100%);" withd="100px">
				</div>
			</div>
		</div>
		</div>
	</div>
	

	<script type="text/javascript" src="<?php echo assets('assets/home/library/owl.carousel.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo assets('assets/home/js/script.min.js') ?>"></script>
	<script type="text/javascript">
		var screen = $(window).innerWidth();
			console.log(screen);
			if (screen >=768) {
		$(window).scroll(function(event) {

				var top = $(this).scrollTop();
				if (top>60) {
					$(".nav").addClass('active_fixed');
				}else{
					$(".nav").removeClass('active_fixed');
				}
			
		});
			}
		$("#search-books").keyup(function(event) {
			var genre = $("#genre-choose").val();
			var keyword = $(this).val();

			$.ajax({
				url: "<?php echo url('sp/ajax/') ?>",
				type: 'POST',
				dataType: 'json',
				data: {"genre": genre,"keyword":keyword},
				success:function(data){
					$(".data-search").html("");
					$(".data-search").css('display', 'block');
					$.each(data, function(index, val) {
						 var li = "<li onclick='setValue(this)' data-slug='"+val['slug']+"'>"+val['name']+"</li>";
						 $(".data-search").append(li);
					});
				}
			})
			
		});
		$("#genre-choose").change(function(event) {
			$(".data-search").html("");
			$(".data-search").css('display', 'none');
		});
		function setValue(self){
			$("#search-books").val($(self).text());
			$(".data-search").css('display', 'none');
			var id = $(self).attr('data-slug');
			$("#search-form").attr('action', "<?php echo url('chi-tiet/') ?>"+id);
			$("#btn-search").attr('type', 'submit');
		}
	</script>
</body>
</html>