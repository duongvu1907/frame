<?php use app\database\DB; ?>
<div class="direct">
	<ul>
		<li><a href="<?php echo url('') ?>">Trang chủ</a></li>
		<li><a href="<?php echo url('sach-moi-nhat') ?>">Tìm kiếm</a></li>
	</ul>
</div>
<h2 class="title text-left h2 text-uppercase" style="padding-left: 20px">Tìm kiếm</h2>
<hr>
<div class="category row">
	<div class="search-list col-3">
		<h2 class="title h2">Trang tìm kiếm</h2>
		<div class="box-category">
			<ul>
				<li class="title-li">Thể loại</li>
				<?php foreach ($genres as $g): ?>
					<li><a href="<?php echo url('the-loai/').name_to_slug($g->name,$g->id) ?>"><?php echo $g->name ?></a></li>
				<?php endforeach ?>
				<li class="text-center"><a class="blue text-center" href="<?php echo url('sach-moi-nhat') ?>">Tất cả</a></li>
			</ul>
		</div>
		<div class="margin20"></div>
		<div class="box-price-search">
			<ul class="text-center">
				<li class="title-li">Nhà xuất bản</li>
				<?php foreach ($publishers as $publisher): ?>

					<li><a class="links" href="<?php echo url('nha-xuat-ban/'.name_to_slug($publisher->name,$publisher->id)) ?>"><?php echo $publisher->name ?> (<?php echo $publisher->count_books ?>) </a></li>
				<?php endforeach ?>

			</ul>
		</div>
		<div class="margin20"></div>
		<div class="box-price-search">
			<ul class="text-center">
				<li class="title-li">Tác giả</li>
				<?php foreach ($authors as $author): ?>

					<li><a class="links" href="<?php echo url('tac-gia/'.name_to_slug($author->name,$author->id)) ?>"><?php echo $author->name ?> (<?php echo $author->count_books ?>) </a></li>
				<?php endforeach ?>

			</ul>
		</div>
	</div>


	<!-- ///////////CONTENT//////// -->
	<div class="content-product col-9">
		<div class="margin50"></div>
		<!-- <div class="banner-category">
			<img src="image/category.png" alt="" width="100%">
			<div class="aos"></div>
		</div> -->
		<div class="filter-list" style="margin-bottom: 20px;">
			<h4>Tìm kiếm theo : </h4>
			<h5><span class="keyword"><?php echo $key ?></span></h5>

		</div>
		<!-- <div class="margin50"></div> -->
		<div class="card">
			<div class="card-header bg-success">
				<h2 class="h2 text-left pad0">Danh sách sản phẩm</h2>
				<div class="option-view">
					<span class="options grid active"><span class="fa fa-th"></span></span>&nbsp;&nbsp;&nbsp;
					<span class="options list"><span class="fa fa-list-ul"></span></span>
				</div>
			</div>
			<div class="card-body" >
				<?php foreach ($books as $book): ?>
					<?php 
					$rateAVG = DB::table("")->query("
					SELECT AVG(rate) as avg_rate,count(book_id) as count_rate FROM reviews inner join books ON
					books.id=reviews.id Where book_id=".$book->id." group by reviews.book_id LIMIT 1
					");
					$rate = isset($rateAVG[0])?$rateAVG[0]:null;
					$author = DB::table("authors")->select(['name'])->where("id","=",$book->author_id)->first();
					$date = date("Y-m-d h:i:s",time());
					$discount = DB::table("")->query("
						SELECT sum(value) as val FROM discounts_books inner join discounts ON
						discounts.id=discounts_books.discount_id and discounts.expiry_at >='".$date."' Where book_id=".$book->id." Group by book_id LIMIT 1
						");
					if ($discount) {
						$discount = $discount[0];
					}else{
						$discount=0;
					}
					?>

					<div class="product col-4" style="margin-top: 10px;padding: 20px">
						<div class="image text-center" style="width: 200px;margin:auto;overflow: hidden;height: 295px">
							<a href="<?php echo url('chi-tiet/').name_to_slug($book->name,$book->id) ?>"><img src="<?php echo assets('upload/books/'.$book->image) ?>" alt="" width="100%"></a>
						</div>

						<div class="content" style="padding-left: 20px;">
							<div class="title-product">
								<h2 class="title-sm"><?php echo $book->name ?></h2>
							</div>
							<div class="author">
								<?php echo $author->name ?>
							</div>
							<div class="description-grid">

							</div>
							<div class="price">
								<span class="new-price">
									<?php if(isset($discount->val)){ ?>
										<?php echo number_format($book->price-$book->price*$discount->val/100); ?><sup>đ</sup>
									<?php }else{ ?>
										<?php echo number_format($book->price) ?><sup>đ</sup></span>
									<?php } ?>
									<span class="percent"><?php echo isset($discount->val)&&$discount->val!=0?"-".$discount->val.'%':"" ?></span><br>
									<span class="old-price"><strike><?php echo isset($discount->val)&&$discount->val!=0?number_format($book->price):"" ?></strike></span>
								</div>
								<div class="rate-star">
									<?php if(!is_null($rate)){ ?>
																<?php for($i=0;$i<$rate->avg_rate;$i++){ ?>
																<span class="fa fa-star" style="color: yellow"></span>
															<?php } ?>
															<span class="comment">(<?php echo $rate->count_rate ?> đánh giá)</span>
															<?php }else{ ?>
															<span class="fa fa-star" style="color: #ebebeb"></span>
															<span class="fa fa-star" style="color: #ebebeb"></span>
															<span class="fa fa-star" style="color: #ebebeb"></span>
															<span class="fa fa-star" style="color: #ebebeb"></span>
															<span class="fa fa-star" style="color: #ebebeb"></span>
															<span class="comment">(0 đánh giá)</span>
														<?php } ?>
								</div>
								<div class="tocart">
									<form action="<?php echo url('add/cart') ?>" method="post">
										<input type="number" name="id" hidden="" value="<?php echo $book->id ?>">
										<input type="number" id="quantity" name="quantity" value="1" hidden="">

										<button class="btn-a btn-a-left color-blue" style="cursor: pointer;">Thêm vào giỏ hàng</button>
									</form>
								</div>
								<div class="is-coupon">

								</div>
							</div>
						</div>
					<?php endforeach ?>

				</div>

			</div>
		</div>
				<hr style="width: 200px;background: blue;height: 2px;float: right;">
				<!-- <div style="clear: both;"></div> -->
				<ul class="pagination" style="margin-right: -150px;">
					<li <?php if($page==0){ echo "class='active'"; } ?>><a href="<?php url(''.$slug) ?>">Trang</a></li>
					<?php for($i=1;$i<=$paginate+1;$i++){ ?>
						<li <?php if($page==$i){ echo "class='active'"; } ?>>
							<a href="<?php echo url(''.$slug.'?page='.$i) ?>">
							<?php echo $i ?>
							</a>
						</li>
					<?php } ?>
				</ul>
	</div>

