<div class="direct">
	<ul>
		<li><a href="<?php echo url('') ?>">Trang chủ</a></li>
		<li><a href="<?php echo url('u/gio-hang') ?>">Giỏ hàng</a></li>
	</ul>
</div>
<h2 class="title text-left h2 text-uppercase" style="padding-left: 20px">Xử lý giỏ hàng</h2>
<hr>
		<form action="<?php echo url('u/gio-hang/luu') ?>" method="post">
<div class="row">
	<div class="col-1"></div>
	<div class="col-10" style="overflow-x: auto;">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>Tên sản phẩm</th>
						<th>Số lượng</th>
						<th>Giá cũ</th>
						<th>Giảm giá</th>
						<th>Giá mới</th>
						<th>Tổng</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php $total=0; ?>
					<?php if(isset($_SESSION["cart"])){ ?>
						<?php foreach ($_SESSION["cart"] as $cart): ?>
							<?php $total+=$cart["price_new"]*$cart["quantity"]  ?>

							<tr>
								<input type="number" value="<?php echo $cart["id"] ?>" name="book_id[]" hidden="">
								<td><?php echo $cart["id"] ?></td>
								<td><?php echo $cart["name"] ?></td>
								<td><div class="quantity">
									<button class="desc" type="button"><span class="fa fa-chevron-down"></span></button>
									<input type="number" id="quantity" name="quantity[]" style="width: 50px;text-align: center" value="<?php echo $cart["quantity"] ?>">
									<button class="asc" type="button"><span class="fa fa-chevron-up "></span></button>

								</div></td>
								<td>1x<?php echo number_format($cart["price_old"]) ?></td>
								<td style="color:red"><?php echo "-".$cart["discount"]."%" ?></td>
								<td class="new_price">1x<?php echo number_format( $cart["price_new"]) ?></td>
								<td><?php echo number_format($cart["price_new"]*$cart["quantity"])  ?></td>
								<td>
									<a href="<?php echo url('cart/delete/'.$cart["id"]) ?>">
										<span class="fa fa-trash" style="color: red;"></span>
									</a>
								</td>
							</tr>
						<?php endforeach ?>
					<?php } ?>
				</tbody>
			</table>

		</div>
		<div class="col-1"></div>
	</div>
	<div class="row">
		<div class="col-7"></div>
		<div class="col-3">
			Tổng hóa đơn : <?php echo number_format($total) ?> VND;
		</div>
	</div>
<div class="row">
	<div class="col-1"></div>
	<div class="col-5">
		<p>Người nhận : <?php echo $user->name ?></p>
		<p>Email : <?php echo $user->email ?></p>
		<a href="<?php echo url('u/ho-so') ?>"><button class="btn btn-success">Thay đổi thông tin tài khoản</button></a>
	</div>
	<div class="col-5">
		<p>Địa chỉ : <?php echo $address ?></p>
		<p>Điện thoại: <?php echo $user->phone ?></p>
		
	</div>
	<div class="col-1"></div>
</div>
<div class="row">
	<div class="col-1"></div>
	<div class="col-5">
		<input type="checkbox" name="address" style="height: 30px;width:30px" id="other_address" > 
		<label for="other_address" style="float: left;margin-top: 10px" >Giao hàng đến địa chỉ khác ?</label>
	</div>
	<div class="col-5"></div>
	<div class="col-1"></div>
</div>
<div class="row tab-address">
	<div class="col-1"></div>
	<div class="col-5">
		<div class="form-group">
			<label>Tỉnh / Thành Phố</label><br>
			<select name="province_id" data-placeholder="Chọn tỉnh thành..." class="chosen-select form-control"   id="province_id">
				<option value=""></option>
				<?php foreach ($provinces as $province): ?>
					<option value="<?php echo $province->id ?>"><?php echo $province->name ?></option>
				<?php endforeach ?>
			</select>
		</div>
		<div class="form-group">
			<label>Xã / Phường</label><br>
			<select name="commune_id" class="chosen-select form-control" tabindex="-1" data-placeholder="Chọn quận huyện ..."  id="commune_id">
			<option value=""></option>
				
			</select>
		</div>
	</div>
	<div class="col-5">
		<div class="form-group">
			<label>Quận / Huyện</label><br>
			<select name="district_id" class="chosen-select form-control" tabindex="-1" data-placeholder="Chọn quận huyện ..."  id="district_id">
			<option value=""></option>

			</select>
		</div>
	</div>
	<div class="col-md-1"></div>
</div>
<div class="row">
		<div class="col-7"></div>
		<div class="col-3">
			<button class="btn btn-primary">Mua</button>
		</div>
	</div>
</form>
<?php if(isset($errors)){ ?>
	<div class="alert">
		<?php foreach ($errors as $error): ?>

			<div class="notify"><?php echo $error ?></div>
		<?php endforeach ?>
		<span class="fa fa-times close"></span>
	</div>
	<?php } ?>
<script type="text/javascript">
	$(".chosen-select").chosen();

	function get_data(id,filter){
		$.ajax({
			url: "<?php echo url('customers/location/') ?>"+filter,
			type: 'POST',
			dataType: 'json',
			data: {"id": id},
			success:function(data){

				switch(filter){
					case "province":
						$("#district_id").html("");
						$("#district_id").html("<option value='' ></option>");
						$.each(data, function(index, val) {
							var option = "<option value='"+val.id+"'>"+val.name+"</option>";
							$("#district_id").append(option);
						});
						$("#district_id").trigger('chosen:updated');
						break;
					case "district":
						$("#commune_id").html("");
						$("#commune_id").html("<option value='' ></option>");
						$.each(data, function(index, val) {
							var option = "<option value='"+val.id+"'>"+val.name+"</option>";
							$("#commune_id").append(option);
						});
						$("#commune_id").trigger('chosen:updated');
					
						break;
				}

			}
		})
		
	}
	$("#province_id").change(function(event) {
		var id = parseInt($(this).val());
		get_data(id,"province");
	});

	$("#district_id").change(function(event) {
		var id = parseInt($(this).val());
		get_data(id,"district");
	});
	$("#other_address").change(function(event) {
		console.log($(this).is(':checked'));
		if ($(this).is(':checked')) {
			$(".tab-address").css('display', 'block');
			$("#province_id").attr('required', 'true');
			$("#district_id").attr('required', 'true');
			$("#commune_id").attr('required', 'true');
		}else{
			$(".tab-address").css('display', 'none');
			$("#province_id").removeAttr('required');
			$("#district_id").removeAttr('required');
			$("#commune_id").removeAttr('required');
		}
	});
	$(".tab-address").css('display', 'none');
	$("#quantity").change(function(event) {
		var quantity = $(this).val();
		var price = $(this).parent().parent().parent().find('.new_price').text();
		console.log(price);
	});
</script>