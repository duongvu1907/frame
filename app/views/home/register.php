<div class="direct">
	<ul>
		<li><a href="<?php echo url('') ?>">Trang chủ</a></li>
		<li><a href="<?php echo url('dang-ky') ?>">Đăng ký</a></li>
	</ul>
</div>
<h2 class="title text-left h2 text-uppercase" style="padding-left: 20px">Đăng Ký</h2>
<hr>

<form action="<?php echo url('dang-ky') ?>" method="post">
<div class="row">
	<div class="col-6">
		<div class="form-group">
			<label>Tên</label>
			<input type="text" name="name" placeholder="Tên của bạn **" class="form-control">
			<span class="validate"></span>
		</div>
		<div class="form-group">
			<label>Mật khẩu</label>
			<input type="password" name="password" placeholder="Mật khẩu của bạn **" class="form-control">
			<span class="validate"></span>
		</div>
		<div class="form-group">
			<label>Tên tài khoản</label>
			<input type="text" name="username" placeholder="Tài khoản của bạn **" class="form-control" class="data-email">
			<span class="validate"></span>
		</div>
		<div class="form-group">
			<label>Giới tính</label>
			<select class="form-control" name="gender" required="">
				<option value="">Chọn giới tính</option>
				<option value="male">Nam</option>
				<option value="female">Nữ</option>
				<option value="other">Khác</option>
			</select>
		</div>
		
	</div>
	<div class="col-6">
		<div class="form-group">
			<label>Email</label>
			<input type="text" name="email" placeholder="Email của bạn **" class="form-control" class="data-email">
			<span class="validate"></span>
		</div>
		<div class="form-group">
			<label>Số điện thoại</label>
			<input type="text" name="phone" placeholder="Số điện thoại của bạn **" class="form-control">
		</div>
		<div class="form-group">
			<label>Tỉnh / Thành Phố</label><br>
			<select name="province_id" data-placeholder="Chọn tỉnh thành..." class="chosen-select form-control"  required="" id="province_id">
				<option value=""></option>
				<?php foreach ($provinces as $province): ?>
					<option value="<?php echo $province->id ?>"><?php echo $province->name ?></option>
				<?php endforeach ?>
			</select>
		</div>
		<div class="form-group">
			<label>Quận / Huyện</label><br>
			<select name="district_id" class="chosen-select form-control" tabindex="-1" data-placeholder="Chọn quận huyện ..." required="" id="district_id">
			<option value=""></option>

			</select>
		</div>
		<div class="form-group">
			<label>Xã / Phường</label><br>
			<select name="commune_id" class="chosen-select form-control" tabindex="-1" data-placeholder="Chọn quận huyện ..." required="" id="commune_id">
			<option value=""></option>
				
			</select>
		</div>
	</div>
</div>
<a href="<?php echo url('dang-ky') ?>" style="margin-left:20px;"><button class="btn btn-success">Đăng ký tài khoản&nbsp;<i class="fa fa-chevron-right"></i></button></a>
</form>
<?php if(isset($errors)){ ?>
<div class="alert">
	<?php foreach ($errors as $error): ?>
		
	<div class="notify"><?php echo $error ?></div>
	<?php endforeach ?>
	<span class="fa fa-times close"></span>
</div>
<?php } ?>
<script type="text/javascript">
	$(".chosen-select").chosen();

	function get_data(id,filter){
		$.ajax({
			url: "<?php echo url('customers/location/') ?>"+filter,
			type: 'POST',
			dataType: 'json',
			data: {"id": id},
			success:function(data){

				switch(filter){
					case "province":
						$("#district_id").html("");
						$("#district_id").html("<option value='' ></option>");
						$.each(data, function(index, val) {
							var option = "<option value='"+val.id+"'>"+val.name+"</option>";
							$("#district_id").append(option);
						});
						$("#district_id").trigger('chosen:updated');
						break;
					case "district":
						$("#commune_id").html("");
						$("#commune_id").html("<option value='' ></option>");
						$.each(data, function(index, val) {
							var option = "<option value='"+val.id+"'>"+val.name+"</option>";
							$("#commune_id").append(option);
						});
						$("#commune_id").trigger('chosen:updated');
					
						break;
				}

			}
		})
		
	}
	$("#province_id").change(function(event) {
		var id = parseInt($(this).val());
		get_data(id,"province");
	});

	$("#district_id").change(function(event) {
		var id = parseInt($(this).val());
		get_data(id,"district");
	});

</script>