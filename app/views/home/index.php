<!--SLIDER - HOT PRODUCT -->
<?php use app\database\DB; ?>

<div class=" grid-container row">
	<div class="product-slider col-8">
		<div class="arrow-slider">
			<div class="arrow arrow-next">
				<span class="fa fa-chevron-right"></span>
			</div>
			<div class="arrow arrow-pre">
				<span class="fa fa-chevron-left"></span>
			</div>
		</div>
		<?php $i=0; ?>
		<?php foreach ($sliders as $key => $slider): ?>
			<?php 
				$uri = "";
				$data = DB::table($slider->filter)->where("id","=",$slider->foreign_key)->first();

				switch ($slider->filter) {
					case "books":
						$uri = "chi-tiet/".name_to_slug($data->name,$data->id);
						break;
					case "authors":
						$uri = "tac-gia/".name_to_slug($data->name,$data->id);
						break;
					case "publishers":
						$uri = "nha-xuat-ban/".name_to_slug($data->name,$data->id);
						break;
					case "genres":
						$uri = "the-loai/".name_to_slug($data->name,$data->id);
						break;
					
				}
			 ?>
		<div class="item <?php if($i==0){ echo 'slide_active';} ?>">
			<div class="image">
				<img src="<?php echo assets('upload/sliders/'.$slider->image) ?>" width="100%" style="height:389px;">
			</div>
			<div class="content">
				<div class="description">
					<h4 class="text-uppercase" style="color:#2874f0;font-size: 19px"><?php echo $slider->title ?></h4>
					<p><?php echo $slider->description ?></p>
				</div>
				<div class="btn-link">
					<a href="<?php echo url($uri) ?>">MUA NGAY</a>
				</div>
			</div>
		</div>
		<?php $i++; ?>
		<?php endforeach ?>

	</div>
	<div class="product-hots col-4">
		<img src="<?php echo assets('upload/features_books/'.$features_books_alone->image) ?>" alt="" width="100%">
		<div class="aos aos-height">
			<div class="link-box">
				<?php $bks = DB::table("books")->where("id","=",$features_books_alone->book_id)->first() ?>
				<a href="<?php echo url('chi-tiet/'.name_to_slug($bks->name,$bks->id)) ?>" class="btn-a-color">MUA NGAY</a>
			</div>
		</div>
			<div class="title-feature-book">
				
				<a href="<?php echo url('chi-tiet/'.name_to_slug($bks->name,$bks->id)) ?> "><?php echo $features_books_alone->title; ?></a>
			</div>
	</div>

</div>
<div class="row">
	<?php foreach ($features_books as $fb): ?>
		<?php $book = DB::table("books")->where("id","=",$fb->book_id)->first() ?>
	<div class="product-hots col-4">
		<img src="<?php echo assets('upload/features_books/'.$fb->image) ?>" alt="" width="100%">
		<div class="aos">
			<div class="link-box">
				<a href="<?php echo url('chi-tiet/'.name_to_slug($book->name,$book->id)) ?>" class="btn-a-color">Mua Ngay</a>
			</div>
		</div>
		<div class="title-feature-book">
				<a href="<?php echo url('chi-tiet/'.name_to_slug($book->name,$book->id)) ?>"><?php echo $fb->title; ?></a>
		</div>
	</div>

	<?php endforeach ?>

</div>
<!-- <div class="hots-category">
	<div class="title-grid">
		
	</div>
	<div class="caroulse-slider">
		<div class="list">
			<div class="image-list">
				<img src="" alt="">
			</div>
			<div class="description-list">

			</div>
		</div>
	</div>
</div> -->
<!-- SLIDER - HOT PRODUCT -->
<div class="featured-product" style="margin-bottom: 100px">
	<div class="card" style="border:none">
		<div class="card-header text-center" style="background:transparent;border: none">
			<h2 class=" title h2 text-uppercase" style="font-weight: bold;padding:0px;padding-top: 20px;color:#de5b18"><strong>Danh Mục Ưa Thích</strong></h2>
			<hr style="width: 250px;height: 2px ;background: #de5b18;border:none;">
		</div>
		<div class="card-body" style="margin: 30px;">
			<div class="arrow-slider">
				<!-- <a href="" class="btn-a btn-a-left radius50 color-blue">Xem tất cả &nbsp;<span class="fa fa-angle-right"></span></a> -->
			</div>
			<div class="caroulse-slider">
				
				<div class="row mini" >

							<?php foreach ($genres_best as $key => $genre): ?>
								<?php $data = ["credit-card","coffee","envelope","book"] ?>
								<div class="col-3">
									<div class="branch">
										<div class="image-branch text-center">
											<span class="fa fa-<?php echo $data[$key]?>"></span>
										</div>
										<div class="description text-center">
											<a href="<?php echo url('the-loai/').name_to_slug($genre->name,$genre->id); ?>"><?php echo $genre->name ?></a>
										</div>
									</div>
								</div>
							<?php endforeach ?>
							




						</div>

				<div class="description-list">

				</div>
			</div>

		</div>
	</div>
</div>
<!-- PRODUCT FEATUTE  -->

<div class="featured-product">
	<div class="card">
		<div class="card-header text-left">
			<h2 class="title-card">
				Sách Mới Ra &nbsp;&nbsp;<span class="fa fa-angle-double-right" ></span>&nbsp;
			</h2>
		</div>
		<div class="card-body">
			<div class="arrow-slider">
				<!-- <a href="" class="btn-a btn-a-left radius50 color-blue">Xem tất cả &nbsp;<span class="fa fa-angle-right"></span></a> -->
			</div>
			<div class="caroulse-slider">
				<div class="row">
					<!-- <div class="col-3">
						<img src="image/right-slide-banner.jpg" alt="" width="100%">
						<div class="aos text-center">
							<div class="link-box">
								<a href="" class="btn-a-color">Xem tất cả</a>
							</div>
						</div>
					</div> -->
					<div class="col-12">
						<div class="owl-carousel owl-theme owl-loaded">
							<div class="owl-stage-outer">
								<div class="owl-stage">
									<?php foreach ($books_new as $book): ?>
										
										<?php 
										$rateAVG = DB::table("")->query("
											SELECT AVG(rate) as avg_rate,count(book_id) as count_rate FROM reviews inner join books ON
											books.id=reviews.id Where book_id=".$book->id." group by reviews.book_id LIMIT 1
											");
										$rate = isset($rateAVG[0])?$rateAVG[0]:null;
										$slug = name_to_slug($book->name,$book->id);
										$author = DB::table("authors")->select(['name'])->where("id","=",$book->author_id)->first();
										$date = date("Y-m-d h:i:s",time());
										$discount = DB::table("")->query("
											SELECT sum(value) as val FROM discounts_books inner join discounts ON
												discounts.id=discounts_books.discount_id and discounts.expiry_at >='".$date."' Where book_id=".$book->id." Group by book_id LIMIT 1
											");
										if ($discount) {
											$discount = $discount[0];
										}else{
											$discount=0;
										}
										?>
										<div class="owl-item">
											<div class="product">
												<div class="image-product">
													<a href="<?php echo url('chi-tiet/'.$slug) ?>"><img src="<?php echo assets('upload/books/'.$book->image) ?>" alt=""></a>
												</div>
												<div class="description">
													<div class="title-product"><?php echo $book->name ?></div>
													<div class="author"><?php echo $author->name ?></div>
													<div class="price">
														<span class="new-price">
															<?php if(isset($discount->val)){ ?>
																<?php echo number_format($book->price-$book->price*$discount->val/100); ?><sup>đ</sup>
															<?php }else{ ?>
																<?php echo number_format($book->price) ?><sup>đ</sup></span>
															<?php } ?>
															<span class="percent"><?php echo isset($discount->val)&&$discount->val!=0?"-".$discount->val.'%':"" ?></span>
															<br>
															<span class="old-price"><del>
																<?php if(isset($discount->val)){ ?>
																<?php echo number_format($book->price) ?></del>
															<?php } ?>
															</span>

														</div>
														<div class="rate-star">
															<?php if(!is_null($rate)){ ?>
																<?php for($i=0;$i<$rate->avg_rate;$i++){ ?>
																<span class="fa fa-star" style="color: yellow"></span>
															<?php } ?>
															<span class="comment">(<?php echo $rate->count_rate ?> đánh giá)</span>
															<?php }else{ ?>
															<span class="fa fa-star" style="color: #ebebeb"></span>
															<span class="fa fa-star" style="color: #ebebeb"></span>
															<span class="fa fa-star" style="color: #ebebeb"></span>
															<span class="fa fa-star" style="color: #ebebeb"></span>
															<span class="fa fa-star" style="color: #ebebeb"></span>
															<span class="comment">(0 đánh giá)</span>
														<?php } ?>
														</div>
														<div class="tocart">
															<form action="<?php echo url('add/cart') ?>" method="post">
																<input type="number" name="id" hidden="" value="<?php echo $book->id ?>">
																<input type="number" id="quantity" name="quantity" value="1" hidden="">

															<button class="btn-a btn-a-left color-blue">Thêm vào giỏ hàng</button>
															</form>
														</div>
													</div>
												</div>
											</div>
										<?php endforeach ?>



									</div>
								</div>

							</div>
						</div>


					</div>

					<div class="description-list">

					</div>
				</div>
			<div class="total text-center" style="padding:20px;">
				<a href="<?php echo url('sach-moi-nhat') ?>">Xem tất cả</a>
			</div>
			</div>
		</div>
	</div>
<div class="featured-product">
	<div class="card">
		<div class="card-header text-left">
			<h2 class="title-card">
				Sách Bán Chạy Nhất &nbsp;&nbsp;<span class="fa fa-angle-double-right" ></span>&nbsp;
			</h2>
		</div>
		<div class="card-body">
			<div class="arrow-slider">
				<!-- <a href="" class="btn-a btn-a-left radius50 color-blue">Xem tất cả &nbsp;<span class="fa fa-angle-right"></span></a> -->
			</div>
			<div class="caroulse-slider">
				<div class="row">
					<!-- <div class="col-3">
						<img src="image/right-slide-banner.jpg" alt="" width="100%">
						<div class="aos text-center">
							<div class="link-box">
								<a href="" class="btn-a-color">Xem tất cả</a>
							</div>
						</div>
					</div> -->
					<div class="col-12">
						<div class="owl-carousel owl-theme owl-loaded">
							<div class="owl-stage-outer">
								<div class="owl-stage">
									<?php foreach ($books_sale_best as $book): ?>
										
										<?php 
										$rateAVG = DB::table("")->query("
											SELECT AVG(rate) as avg_rate,count(book_id) as count_rate FROM reviews inner join books ON
											books.id=reviews.id Where book_id=".$book->id." group by reviews.book_id LIMIT 1
											");
										$rate = isset($rateAVG[0])?$rateAVG[0]:null;
										$slug = name_to_slug($book->name,$book->id);
										$author = DB::table("authors")->select(['name'])->where("id","=",$book->author_id)->first();
										$date = date("Y-m-d h:i:s",time());
										$discount = DB::table("")->query("
											SELECT sum(value) as val FROM discounts_books inner join discounts ON
												discounts.id=discounts_books.discount_id and discounts.expiry_at >='".$date."' Where book_id=".$book->id." Group by book_id LIMIT 1
											");
										if ($discount) {
											$discount = $discount[0];
										}else{
											$discount=0;
										}
										?>
										<div class="owl-item">
											<div class="product">
												<div class="image-product">
													<a href="<?php echo url('chi-tiet/'.$slug) ?>"><img src="<?php echo assets('upload/books/'.$book->image) ?>" alt=""></a>
												</div>
												<div class="description">
													<div class="title-product"><?php echo $book->name ?></div>
													<div class="author"><?php echo $author->name ?></div>
													<div class="price">
														<span class="new-price">
															<?php if(isset($discount->val)){ ?>
																<?php echo number_format($book->price-$book->price*$discount->val/100); ?><sup>đ</sup>
															<?php }else{ ?>
																<?php echo number_format($book->price) ?><sup>đ</sup></span>
															<?php } ?>
															<span class="percent"><?php echo isset($discount->val)&&$discount->val!=0?"-".$discount->val.'%':"" ?></span>
															<br>
															<span class="old-price"><del>
																<?php if(isset($discount->val)){ ?>
																<?php echo number_format($book->price) ?></del>
															<?php } ?>
															</span>

														</div>
														<div class="rate-star">
															<?php if(!is_null($rate)){ ?>
																<?php for($i=0;$i<$rate->avg_rate;$i++){ ?>
																<span class="fa fa-star" style="color: yellow"></span>
															<?php } ?>
															<span class="comment">(<?php echo $rate->count_rate ?> đánh giá)</span>
															<?php }else{ ?>
															<span class="fa fa-star" style="color: #ebebeb"></span>
															<span class="fa fa-star" style="color: #ebebeb"></span>
															<span class="fa fa-star" style="color: #ebebeb"></span>
															<span class="fa fa-star" style="color: #ebebeb"></span>
															<span class="fa fa-star" style="color: #ebebeb"></span>
															<span class="comment">(0 đánh giá)</span>
														<?php } ?>
														</div>
														<div class="tocart">
															<form action="<?php echo url('add/cart') ?>" method="post">
																<input type="number" name="id" hidden="" value="<?php echo $book->id ?>">
																<input type="number" id="quantity" name="quantity" value="1" hidden="">

															<button class="btn-a btn-a-left color-blue">Thêm vào giỏ hàng</button>
															</form>
														</div>
													</div>
												</div>
											</div>
										<?php endforeach ?>



									</div>
								</div>

							</div>
						</div>


					</div>

					<div class="description-list">

					</div>
				</div>

			</div>
		</div>
	</div>


	<div class="featured-product">
		<div class="card">
			<div class="card-header text-left">
				<h2 class="title-card text-center">
					Nhà xuất bản nổi bật
				</h2>
			</div>
			<div class="card-body">
				<div class="arrow-slider">
					<!-- <a href="" class="btn-a btn-a-left radius50 color-blue">Xem tất cả &nbsp;<span class="fa fa-angle-right"></span></a> -->
				</div>
				<div class="caroulse-slider">
					<!-- <div class="row">
						<div class="col-3">
							<img src="image/right-slide-banner.jpg" alt="" width="100%">
							<div class="aos text-center">
								<div class="link-box">
									<a href="" class="btn-a-color">Xem tất cả</a>
								</div>
							</div>
						</div> -->
						<div class="col-12">
							<div class="owl-carousel owl-theme owl-loaded">
								<div class="owl-stage-outer">
									<div class="owl-stage">
										<?php foreach ($publishers as $publisher): ?>
											
											<div class="owl-item">
												<div class="product">
													<div class="image-product">
														<a href="<?php echo url('nha-xuat-ban/'.name_to_slug($publisher->name,$publisher->id)) ?>"><img src="<?php echo assets('upload/publishers/'.$publisher->image) ?>" alt=""></a>
													</div>
													<div class="description text-uppercase text-center" style="padding:10px;">
														<a href="<?php echo url('nha-xuat-ban/'.name_to_slug($publisher->name,$publisher->id)) ?>"><?php echo $publisher->name ?></a>
													</div>
												</div>
											</div>
										<?php endforeach ?>
										



									</div>
								</div>

							</div>
						</div>


					</div>

					<div class="description-list">

					</div>
				</div>

			</div>
		</div>
	</div>
	<div class="banner-">
		<div class="row">
			<div class="col-6">

			</div>
			<div class="col-6">

			</div>
		</div>
	</div>


	<!-- PRODUCT FEATUTE --->