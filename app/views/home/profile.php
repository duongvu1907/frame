<?php use app\database\DB;
	use app\core\Auth;
 ?>
<div class="direct">
	<ul>
		<li><a href="<?php echo url('') ?>">Trang chủ</a></li>
		<li><a href="<?php echo url('u/ho-so')?>">Hồ sơ tài khoản</a></li>
	</ul>
</div>
<h2 class="title text-left h2 text-uppercase" style="padding-left: 20px">Tài khoản : <?php echo Auth::customer()->username   ?></h2>
<br>
<a href="<?php echo url('dang-xuat') ?>"><button class="btn btn-primary">Đăng Xuất ?</button></a>
<hr>
<div class="row">
	<form action="<?php echo url('u/ho-so/edit') ?>" method="post">
		<div class="col-8" style="padding: 20px">
			<div class="form-group">
				<label>Tên</label>
				<input type="text" name="name" placeholder="Tên của bạn **" class="form-control" value="<?php echo $user->name ?>">
			</div>
			<div class="form-group">
				<label>Email</label>
				<input type="text" name="email" placeholder="Email của bạn **" class="form-control"  value="<?php echo $user->email ?>" disabled="">
			</div>
			<div class="form-group">
				<label>Tài khoản</label>
				<input disabled="" type="text" name="username" placeholder="Tên của bạn **" class="form-control"  value="<?php echo $user->username ?>">
			</div>
			<div class="form-group">
				<label>Điện thoại</label>
				<input type="text" name="phone" placeholder="Tên của bạn **" class="form-control"  value="<?php echo $user->phone ?>">
			</div>
			<div class="form-group">
			<label>Tỉnh / Thành Phố</label>
			<select name="province_id" data-placeholder="Chọn tỉnh thành..." class="chosen-select form-control"  required="" id="province_id">
				<option value=""></option>
				<?php foreach ($provinces as $p): ?>
					<option value="<?php echo $p->id ?>" <?php if($p->id==$province_id) echo "selected" ?>><?php echo $p->name ?></option>
				<?php endforeach ?>
			</select>
		</div>
		<div class="form-group">
			<label>Quận / Huyện</label>
			<select name="disctrict_id" class="chosen-select form-control" tabindex="-1" data-placeholder="Chọn quận huyện ..." required="" id="disctrict_id">
			<option value=""></option>
				<?php foreach ($districts as $p): ?>
					<option value="<?php echo $p->id ?>" <?php if($p->id==$district_id) echo "selected" ?>><?php echo $p->name ?></option>
				<?php endforeach ?>
			</select>
		</div>
		<div class="form-group">
			<label>Xã / Phường</label>
			<select name="commune_id" class="chosen-select form-control" tabindex="-1" data-placeholder="Chọn quận huyện ..." required="" id="commune_id">
			<option value=""></option>
				<?php foreach ($communes as $p): ?>
					<option value="<?php echo $p->id ?>" <?php if($p->id==$commune_id) echo "selected" ?>><?php echo $p->name ?></option>
				<?php endforeach ?>
			</select>
		</div>
			<div class="form-group">
				<label>Giới tính</label>
				<select class="form-control" name="gender">
					<option value="male" <?php if($user->gender=="male"){ echo "selected";} ?>>Nam</option>
					<option value="female" <?php if($user->gender=="female"){ echo "selected";} ?>>Nữ</option>
					<option value="other" <?php if($user->gender=="other"){ echo "selected";} ?>>Khác</option>
				</select>
			</div>
			<div class="form-group">
				<input type="checkbox" id="change_password" name="check_pass"> <label for="change_password">Thay đổi mật khẩu</label>
			</div>
			<div class="pass" style="display: none;">
				<div class="form-group">
					<label>Mật khẩu cũ</label>
					<input type="password" name="password" placeholder="Mật khẩu hiện tại của bạn **" class="form-control pass-once">
				</div>
				<div class="form-group">
					<label>Mật khẩu mới</label>
					<input type="password" name="new_password" placeholder="Mật khẩu mới của bạn **" class="form-control pass-once">
				</div>
			</div>
			<div class="form-group">
				<button class="btn btn-primary">Sửa</button>
			</div>
		</div>
	</form>
	<div class="col-4">
		<h2 class="title text-left h2 text-uppercase" style="padding-left: 20px">Lịch sử giao dịch </h2>
<hr>
	<div class="tab">
		<?php foreach ($orders as $order): ?>
			<?php $orders_details = DB::table("orders_details")->where("order_id","=",$order->id)->get() ?>
		<div class="option-order">
			<h4 class="head bg-success text-white">Mua hàng <?php echo $order->created_at ?> <sup>&nbsp;&nbsp;
				<?php if($order->status!=0){ ?>
				<span class="fa fa-check text-success" style="font-size: 20px;"></span></sup>
				<?php }else{ ?>
				<span class="fa fa-spinner text-danger" style="font-size: 20px;"></span></sup>
		
				<?php } ?>
			</h4>
			<div class="order-detail">
				<ul>
					<li>Số tiền : <?php echo isset($order->amount)?number_format($order->amount)."<sup>đ</sup>":"Chưa xử lý" ?>  </li>
					<li>Trạng thái : <?php echo $order->status!=0?"Đã gửi":"Đang xử lý" ?></li>
					<li>Ngày mua : <?php echo $order->created_at ?></li>
					<li>Địa chỉ : <?php echo $order->address ?></li>
					<li>Chi tiết :
						<ul class="sub-order">
							<?php foreach ($orders_details as $od): ?>
								<?php $book = DB::table("books")->select(["name"])->where("id","=",$od->book_id)->first() ?>
							<li> <?php echo $book->name ?>
								<br> <i style="font-size: 14px;margin-left: 10px;">(<?php echo $od->quantity; ?>x<?php echo number_format($od->price) ?> <sup>đ</sup>)</i>
							</li>
							<?php endforeach ?>
						</ul>
					 </li>
				</ul>
			</div>
		</div>
		<?php endforeach ?>
	
	</div>
	</div>
</div>
<div class="row">
	
</div>
<?php if(isset($errors)){ ?>
<div class="alert">
	<?php foreach ($errors as $error): ?>
		
	<div class="notify"><?php echo $error ?></div>
	<?php endforeach ?>
	<span class="fa fa-times close"></span>
</div>
<?php } ?>
<script type="text/javascript">
	$(".chosen-select").chosen();

	function get_data(id,filter){
		$.ajax({
			url: "<?php echo url('customers/location/') ?>"+filter,
			type: 'POST',
			dataType: 'json',
			data: {"id": id},
			success:function(data){

				switch(filter){
					case "province":
						$("#disctrict_id").html("");
						$("#disctrict_id").html("<option value='' ></option>");

						$.each(data, function(index, val) {
							var option = "<option value='"+val.id+"'>"+val.name+"</option>";
							$("#disctrict_id").append(option);
						});
						$("#disctrict_id").trigger('chosen:updated');
						break;
					case "district":
						$("#commune_id").html("");
						$("#commune_id").html("<option value='' ></option>");
						$.each(data, function(index, val) {
							var option = "<option value='"+val.id+"'>"+val.name+"</option>";
							$("#commune_id").append(option);
						});
						$("#commune_id").trigger('chosen:updated');
					
						break;
				}

			}
		})
		
	}
	$("#province_id").change(function(event) {
		var id = parseInt($(this).val());
		$("#commune_id").html("");
		$("#commune_id").html("<option value='' ></option>");
		get_data(id,"province");
	});

	$("#disctrict_id").change(function(event) {
		var id = parseInt($(this).val());
		get_data(id,"district");
	});

</script>
<script type="text/javascript">
	$("#change_password").change(function(event) {
		var value = $(this).val();
		if ($(this).is(':checked')) {
			$(".pass").css('display', 'block');
			$(".pass-once").attr('required', 'true');
		}else{
			$(".pass").css('display', 'none');
			$(".pass-once").removeAttr('required');

		}
	});
	var order = $(".option-order");
	$(".option-order").click(function(event) {
		if ($(this).hasClass('active')) {
			$(this).removeClass('active');
		}else{
			removeActive(order);
		$(this).addClass('active');
		}
	});
	function removeActive(order){
		for (var i = 0; i < order.length; i++) {
			order.eq(i).removeClass('active');
		}
	}
</script>