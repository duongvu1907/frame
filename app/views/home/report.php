<?php use app\database\DB; ?>
<div class="direct">
	<ul>
		<li><a href="<?php echo url('') ?>">Trang chủ</a></li>
		<li><a href="<?php echo url('u/ho-so')?>">Hồ sơ</a></li>
	</ul>
</div>
<h2 class="title text-left h2 text-uppercase" style="padding-left: 20px">Mã đơn hàng : MS<?php echo $order_id ?></h2>
<hr>
<div class="row" style="margin-top: 20px;">
	<div class="col-3"></div>
	<div class="col-6 card" >
		<div class="card-header">
			<h4 style="padding: 20px">Đặt hàng thành công</h4>
		</div>

		<div class="car-body">
			<ul style="padding: 20px;">
				<?php $total = 0; ?>
				<?php foreach ($details as $d): ?>
					<?php $total+=$d->quantity*$d->price; ?>
				<?php $book = DB::table("books")->where("id","=",$d->book_id)->first() ?>	
				<li style="padding: 5px"> Tên sách : <?php echo $book->name ?> - <?php echo number_format($d->price) ?><sup>đ</sup> x <?php echo $d->quantity ?> </li>
				<?php endforeach ?>
				<li style="padding: 5px">Tổng đơn hàng : <?php echo number_format($total); ?> VND</li>
			</ul>
			<p class="text-success" style="padding: 10px 20px;">Đang xử lý</p>
			<hr>
			<ul style="padding: 20px">
				<li style="padding: 5px">Tên khách hàng : <?php echo $user->name ?></li>
				<li style="padding: 5px">Địa chỉ  : <?php echo $address ?></li>
				<li style="padding: 5px">Số điện thoại  : <?php echo $user->phone ?></li>
			</ul>
		</div>
	</div>
	<div class="col-3"></div>
</div>
<div class="row">
	<div class="col-3"></div>
	<div class="col-6">
		<p class="text-center">Quay lại trang chủ <span class="timeout"></span></p>
	</div>
	<div class="col-3"></div>
</div>

<script type="text/javascript">
	var i = 30;
	setInterval(function(){
			$(".timeout").text("( "+i+" )");
			i--;
			if (i==0) {
				window.location.href = "<?php echo url('') ?>";
			}
	},1000)
</script>