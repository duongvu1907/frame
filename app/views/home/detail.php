<div class="direct">
	<ul>
		<li><a href="<?php echo url('') ?>">Trang chủ</a></li>
		<li><a href="<?php echo url('chi-tiet/').name_to_slug($book->name,$book->id) ?>"><?php echo $book->name ?></a></li>
	</ul>
</div>
<h2 class="title text-left h2 text-uppercase" style="padding-left: 20px">Chi tiết sản phẩm : <?php echo $book->name ?></h2>
<hr>
<div class="row">
	<div class="col-1"></div>
	<div class="col-5" style="padding: 20px">
		<div class="image" style="max-width: 310px;margin:auto">
			<img src="<?php echo assets('upload/books/'.$book->image) ?>" alt="" width="100%">
		</div>
	</div>
	<div class="col-5">
		<div class="product-preview">
			<div class="product-branch">
				<span><?php echo $publisher->name ?></span>
			</div>
			<div class="product-name">
				<h2><?php echo $book->name ?></h2>
			</div>
			<div class="rate">
				<?php if($rateAVG!=null){ ?>
				<?php for($i=1;$i<=$rateAVG->avg_rate;$i++){ ?>
					<span class="fa fa-star" style="color:yellow"></span>
				<?php } ?>
				<i style="font-size: 13px;">( <?php echo $rateAVG->count_rate ?> đánh giá )</i>
			<?php }else{ ?>
				<span class="fa fa-star"></span>
				<span class="fa fa-star"></span>
				<span class="fa fa-star"></span>
				<span class="fa fa-star"></span>
				<i style="font-size: 13px;">( 0 đánh giá )</i>
			<?php } ?>
				<!-- <button class="">viet binh luan</button> -->
			</div>
			<div class="price">
				<?php 
					if (isset($discount->val)&&(int)$discount->val!=0) {
						?>
						<div class="new-price">
							<span><?php echo number_format((float)$book->price-(float)$discount->val*(float)$book->price/100) ?></span>
							<span class="text-danger" style="color:red;font-size: 13px;margin-left: 30px;"> <?php echo "-".$discount->val."%"; ?></span>
						</div>
						<div class="old-price" style="color:#e4e4e4">
							<del><?php echo number_format($book->price) ?></del>
						</div>
						<?php
					}else{
				 ?>
				 	<div class="new-price">
				 		<span><?php echo number_format($book->price) ?></span>
				 	</div>
				<?php } ?>
			</div>
			<div class="property">
				<dt>Tác giả :</dt>
				<dd><?php echo $author->name ?></dd>
				<dt>Thể loại : </dt>
				<dd><?php 

					foreach ($genres as $genre) {
						echo $genre->name.",";
					}

				 ?></dd>
				<dt>Nhà sản xuất :</dt>
				<dd><?php echo $publisher->name ?></dd>
				<dt>Số lượng còn lại :</dt>
				<dd><?php echo $book->quantity ?></dd>
			</div>
			<form action="<?php echo url('add/cart') ?>" method="post">
				<input type="number" name="id" hidden="" value="<?php echo $book->id ?>">
			<div class="quantity">
				<button type="button"  class="desc"><span class="fa fa-chevron-down"></span></button>
				<input type="number" id="quantity" name="quantity" value="1" min="1">
				<button type="button" class="asc"><span class="fa fa-chevron-up "></span></button>

			</div>
			<button class="btn btn-primary" style="margin-top: 50px;">Thêm vào giỏ hàng</button>
		</form>
			
		</div>
	</div>
	<div class="col-1"></div>
</div>
<div class="row">
	<div class="col-8">
		<h2 class="title text-left h2 text-uppercase" style="padding-left: 20px">Mô tả sản phẩm : </h2>
		<hr>
		<div class="description">
				<?php echo html_entity_decode($book->description) ?>
			</div>
	</div>
	<div class="col-4">
		<h2 class="title text-left h2 text-uppercase" style="padding-left: 20px">Bình luận : </h2>
		<hr>
		<div class="comment">
			<?php if(!isset($_SESSION["username"])){ ?>
				<div class="login">
					<a href="<?php echo url('dang-nhap') ?>"><button class="btn btn-primary">Đăng nhập</button> <span>Bạn chưa đăng nhập</span></a>
				</div>
			<?php }else{ ?>
				<form action="<?php echo url('sach/danh-gia') ?>" method="post">
					<div class="star" style="color:#ebebeb;cursor: pointer;">
						<span class="fa fa-star" data-point="1"></span>
						<span class="fa fa-star" data-point="2"></span>
						<span class="fa fa-star" data-point="3"></span>
						<span class="fa fa-star" data-point="4"></span>
						<span class="fa fa-star" data-point="5"></span>
					</div>
					<input type="number" id="point" class="rate" name="rate" hidden="" value="0">
					<input type="text" class="form-control" name="content">
					<input type="id" id="id" name="book_id" hidden="" value="<?php echo $book->id ?>">
					<button class="btn btn-success">Bình luận</button>
				</form>
			<?php } ?>
			<?php foreach ($reviews as $review): ?>
				<div class="rate-star">
				<span class="username"><i style="font-size: 12px">( <?php echo $review->created_at ?> )</i> <?php echo $review->username; ?>
				<?php for($i=0;$i<(int)$review->rate;$i++){ ?>
					<span class="fa fa-star" style="color: yellow"></span>
				<?php } ?>
			</span>
				<span class="value"><?php echo $review->content ?></span>
			</div>
			<?php endforeach ?>
			

		</div>
	</div>
	<!-- <div class="col-2"></div> -->
</div>
<script type="text/javascript">
	$(".comment .star span").click(function(event) {
		var point = $(this).attr('data-point');
		// alert(point);
		for (var i = 0; i < 5; i++) {
			$(".star").find('span.fa').eq(i).css('color', '#ebebeb');
		}
		for (var i = 0; i < point; i++) {
			$(".star").find('span.fa').eq(i).css('color', 'yellow');
		}

		$("#point").val(point)
});
</script>