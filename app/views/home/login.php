<div class="direct">
	<ul>
		<li><a href="/">Trang chủ</a></li>
		<li><a href="/">Đăng nhập</a></li>
	</ul>
</div>
<h2 class="title text-left h2 text-uppercase" style="padding-left: 20px">Đăng nhập</h2>
<hr>
<form action="<?php echo url('dang-nhap') ?>" method="post">
<div class="row">
	<div class="col-5">

		<div class="row">
			<div class="col-12">
				<input type="text" class="form-control" name="username" placeholder="Tên tài khoản ***">
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<input type="password" class="form-control" name="password" placeholder="Mật khẩu ***">
			</div>
		</div>
		<div class="row">
			<div class="col-4">
				<button class="btn btn-primary">Đăng nhập</button>
			</div>
			<div class="col-4" style="line-height: 40px;font-size: 14px;">
				<a href="<?php echo url('u/xac-minh') ?>" class="hover-a">Quên mật khẩu ?</a>
			</div>
		</div>
	</div>
	<div class="col-2"></div>
	<div class="col-5">
		<div class="panel-body bg-card card-pad">
                        <p class="new-customer-intro">Lợi ích của việc tạo tài khoản:</p>
                        <ul class="new-customer-fact-list">
                            <li class="new-customer-fact">Thanh toán nhanh hơn</li>
                            <li class="new-customer-fact">Thuận tiện giao hàng</li>
                            <li class="new-customer-fact">Truy cập lịch sử </li>
                            <li class="new-customer-fact">Theo dõi đơn hàng</li>
                        </ul>
                        <a href="<?php echo url('dang-ky') ?>"><button class="btn btn-success" type="button">Đăng ký tài khoản &nbsp;<i class="fa fa-chevron-right"></i></button></a>
                    </div>
	</div>
</div>
</form>
<?php if(isset($errors)){ ?>
<div class="alert">
	<?php foreach ($errors as $error): ?>
		
	<div class="notify"><?php echo $error ?></div>
	<?php endforeach ?>
	<span class="fa fa-times close"></span>
</div>
<?php } ?>