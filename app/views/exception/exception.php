<!DOCTYPE html>
<html>
<head>
	<title>Exception <?php echo $exception->getMessage() ?></title>
	<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	 <style type="text/css">
	 	body{
	 		padding: 0;margin:0;
	 		overflow-x: hidden;
	 		font-family: 'Poppins',san serif;
	 	}
	 	.text-center{text-align:center;}
	 	.title{text-transform:capitalize;font-weight: 500;}
	 .fixed{
	 	/*position:absolute;*/
	 	width: 400px;
	 	top:0px;
	 	left:0px;
	 	height:100%;
	 	background:rgba(0,0,0,.9);
	 	z-index: -1;
	 	padding-top: 100px 0px;
	 	margin: 0px;

	 }
	 .content{
	 	padding: 20px;
	 	overflow-y: scroll;
	 	/*overflow-x: visible;*/
	 	width: 100%;
	 	height:800px;
	 	background:rgba(0,0,0,.9);
	 	color:#fff;
	 }
	 ul{
	 	width: 100%;
	 }
	 li{
	 	list-style: none;
	 	font-size: 16px;
	 }
	 pre {
    background: #333;
    white-space: pre;
    word-wrap: break-word;
    overflow: auto;
    border:none;
    color:#4dd0e1;
}

pre.code {
    margin: 20px 25px;
    border-radius: 4px;
    border: 1px solid #292929;
    position: relative;
}


pre::after {
    content: "double click to selection";
    padding: 0;
    width: auto;
    height: auto;
    position: absolute;
    right: 18px;
    top: 14px;
    font-size: 12px;
    color: #ddd;
    line-height: 20px;
    overflow: hidden;
    -webkit-backface-visibility: hidden;
    transition: all 0.3s ease;
}

pre:hover::after {
    opacity: 0;
    visibility: visible;
}
	 </style>  
</head>
<body>

	<div class="card">
		<div class="card-header bg-primary" style="padding: 20px;">
			<h3 class="title"><?php echo "Code : ".$exception->getCode()." <span class='fa fa-exclamation-triangle'></span> ".$exception->getMessage() ?></h3>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-md-4" style="padding-right: 0px;margin-right: 0px">
					<ul class="content">
						<li>
							<p><span class="fa fa-folder"></span>&nbsp;<?php echo $exception->getFile() ?></p>
						</li>
						<li>
							<p><span class="fa fa-file-word"></span>&nbsp;<?php echo "Line : ".$exception->getLine() ?></p>
						</li>
						<?php $lines = array(); ?>
						<?php foreach ($exception->getTrace() as $rows): ?>
							<?php if (is_array($rows)) { ?>
								<?php foreach ($rows as $key => $value): ?>
									<?php if (!is_array($value)) {?>
										<li>
											<?php if($key=='line'){ array_push($lines,$value);} ?>
											<span class="fa fa-angle-right"></span>
											<?php echo $key." => ".$value ?>
										</li>
									<?php }else{ ?>

								     <li> <?php echo $key." : {" ?>
								     	<ul>

								     		<?php foreach ($value as $k => $v): ?>
								     			<li style="font-size: 14px;">
													<span class="fa fa-feather-alt"></span>
													<?php echo isset($key)&&!is_array($v)?$k." => ".$v:"" ?>
												</li>
								     		<?php endforeach ?>
								     	</ul>
								     	<span style="margin-left: 20px;">}</span>
								     </li>
									<?php } ?>
								<?php endforeach ?>
							<?php } ?>
						<?php endforeach ?>
					</ul>
				</div>
				<div class="col-md-8" style="float: right;margin-top:0px;padding:0px;margin-left: 0px">
					
					<?php $file = fopen($exception->getFile(),'r');
						if ($file) {
							$i=1;
							while (($line = fgets($file)) != false) {
								if ($line!=="") {
									if ($i==$exception->getLine()) {
										echo "<code ><pre style='background:#dc2e2e'>";
										echo $i."&nbsp; &nbsp;";
										print_r($line);
										echo "</pre></code>";
										$i++;
									}else{
										echo "<code><pre>";
										echo $i."&nbsp; &nbsp;";
										print_r($line);
										echo "</pre></code>";
										$i++;
									}
									
								}
							}
						}
					 ?>
				</div>
			</div>
		</div>
	</div>
	
</body>
</html>