<?php
/**
 * 
 */
namespace app\controllers\auth;

use app\core\Controller;
use app\core\Registry;
use app\validator\Request;
use app\core\Auth;
use app\database\DB;
class AdminController extends Controller
{
	function __construct()
	{
		parent::__construct();
		$this->layout("auth");
	}
	public function login(){
		if (isset($_SESSION["email"])) {
			// var_dump(Registry::getInstance()->auth); 
			return $this->redirect(url("admin/dashboard"));			
		}
		return $this->view("auth.login");
	}
	public function do_login(){
		$request = new Request;
		$validated = $request->validated([
			"email"=>"required|email"
		]);
		if (!is_null($validated) && !empty($validated)) {
			$error = "?";
			foreach ($validated as $key => $value) {
				if (count($validated)==$key+1) {
					$error.="ms_$key=\"$value\"";
				}else{
					$error.="ms_$key=\"$value\"&";
				}
			}
			return $this->redirect($request->reference.$error);
		}
		$email = $request->email;

		$remember = $request->remember=="on"?1:0;
		$password = trim($request->password);
		$user = DB::table("users")->where("email","LIKE","\"".$request->email."\"")->first();
		if ($user) {
			
			if (password_verify($password,$user->password)) {
				
				$_SESSION["email"] = $user->email;
				// if ($remember===1) {
				// 	$value = $request->email.",".$request->password;
				// 	setcookie("email",$value, time() + 60*60*24*30);
				// }
				// Registry::getInstance()->auth = $user;

				return $this->redirect(url('admin/dashboard'));
			}
		}
		
		// var_dump(Registry::getInstance()->auth);
		return $this->redirect(url('admin/login'));
	}
	public function register(){
		return $this->view("auth.register");
	}
	public function do_register(){
		$request = new Request;
		$validated = $request->validated([
			"name"=>"required",
			"email"=>"required|email|unique:users",
			"password"=>"required"
		]);
		// var_dump($validated);
		if (!is_null($validated) && !empty($validated)) {
			$error = "?";
			foreach ($validated as $key => $value) {
				if (count($validated)==$key+1) {
					$error.="ms_$key=\"$value\"";
				}else{
					$error.="ms_$key=\"$value\"&";
				}
			}
			return $this->redirect($request->reference.$error);
		}
		$date = date("Y-m-d h:i:s",time());
		// echo password_hash($request->password,PASSWORD_DEFAULT);
		DB::table("users")->insert([
			"name"=>$request->name,
			"email"=>$request->email,
			"password"=>password_hash(trim($request->password), PASSWORD_DEFAULT),
			"created_at"=>$date,
			"updated_at"=>$date
		]);
		return $this->redirect(url('admin/login'));

	}
	public function logout(){
		if (isset($_SESSION["email"])){
			unset($_SESSION["email"]);
		}
		// Registry::getInstance()->auth = null;
		return $this->redirect(url('admin/login'));
	}
	public function forgetPassword(){
		return $this->view("auth.forgetPassword");
	}
	public function send_token(){
		$request = new Request;
		$email = $request->email;
		$user = DB::table("users")->where("email","LIKE","\"".$email."\"")->first();
		if (!$user) {
			return $this->redirect($request->reference."?ms_0='email is not exist'");
		}
		$token = str_random(32);
		// var_dump(sendMail($request->email,"verify","Xác nhận tài khoản Book Store",["token"=>$token]));
		if (sendMail($request->email,"verify","Xác nhận tài khoản Book Store",["token"=>$token])) {
			DB::table("users")->where("id","=",$user->id)->update([
				"remember_token"=>$token,
				"updated_at"=>date("Y-m-d h:i:s",time())
			]);
			// echo 'string';
			return $this->redirect(url("admin/login/verify/".$user->id));
		}

	}
	public function check_token($id){
		// $user = DB::table("users")->where("id","=",$id)->first();
		return $this->view("auth.verify",[
			"id"=>$id
		]);
	}
	public function checked_token($id){
		$request = new Request;
		$user = DB::table("users")->where("id","=",$id)->first();
		if ($request->token===$user->remember_token) {
			$_SESSION["token"] = $request->token;
			return $this->view("auth.change_password",[
				"id"=>$id
			]);
		}
		return $this->redirect(url("admin/login/verify/".$id."?ms_0=''"));
	}
	public function change_password($id){
		$request = new Request;
		if (isset($_SESSION["token"])&&!empty($_SESSION["token"])) {
			$password = password_hash(trim($request->password),PASSWORD_DEFAULT);
			$user = DB::table("users")->where("id","=",$id)->update([
				"password"=>$password
			]);
			unset($_SESSION["token"]);
		}
		return $this->redirect(url('admin/login'));
	}
	public function profile(){
		$this->layout("admin");
		$user = Auth::user();
		return $this->view('auth.profile',[
			"user"=>$user
		]);
	}
	public function save_profile(){
		$request = new Request;
		$id = Auth::user()->id;
		if ($request->password) {
			$password = password_hash(trim($request->password), PASSWORD_DEFAULT);
			DB::table("users")->where("id","=",$id)->update([
				"password"=>$password
			]);
			unset($_SESSION["email"]);
			return $this->redirect(url('admin/login'));
		}
		DB::table("users")->where("id","=",$id)->update([
			"name"=>$request->name,
		]);
		return $this->redirect(url('admin/profile'));
	}
	public function search(){
		$this->layout("admin");
		return $this->view("admin.search", []);
	}
	public function search_input(){
		$request = new Request;
		$data = DB::table($request->filter)->where("name","LIKE","\"%".$request->keyword."%\"")->get();
		die(json_encode($data));
	}
}