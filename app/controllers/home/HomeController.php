<?php
/**
 * 
 */
namespace app\controllers\home;

use app\core\Controller;
use app\core\Registry;
use app\core\Auth;
use app\validator\Request;
use app\database\DB;
class HomeController extends Controller
{
	function __construct()
	{

		$this->layout("home");
		parent::__construct();
	}
	public function index(){

		$data["action"] = "index";
		$books_new = DB::table("books")->orderBy("id","DESC")->limit(15)->get();
		$publishers = DB::table("publishers")->query("
			SELECT publishers.id,publishers.name,publishers.image ,count(books.id) as cc FROM publishers inner join 
				books ON publishers.id=books.publisher_id Group By publisher_id Order by cc DESC
			");

		// $genres = DB::table("")->query("
		// 	SELECT genre_id ,count(book_id) as count_book FROM genres_books
		// 	Group By genre_id Order by count_book DESC LIMIT 4
		// 	");
		$books_sale_best = DB::table("")->query(
			"SELECT books.id,books.author_id,books.image,books.name,books.price,count(book_id) as orders_number FROM books inner join orders_details ON orders_details.book_id = books.id group by books.id order by orders_number DESC LIMIT 15"
		); 

		$genres_best = DB::table("")->query(
			"SELECT * FROM genres where id IN (SELECT genre_id from genres_books where book_id IN (SELECT books.id  FROM books inner join orders_details ON orders_details.book_id = books.id group by books.id order by count(book_id) DESC) GROUP BY genre_id ) LIMIT 4"
		);

		$sliders = DB::table("sliders")->orderBy('id','DESC')->get();

		$features_books = DB::table("features_books")->orderBy('id','DESC')->limit(3)->offset(1)->get();
		$features_books_alone = DB::table("features_books")->orderBy('id','DESC')->first();

		$genres_new = DB::table("genres")->orderBy("id","DESC")->limit(5)->get();

		// var_dump($books_sale_best);
		return $this->view("home.index",[
				"sliders"=>$sliders,
				"books_new"=>$books_new,
				"genres_new"=>$genres_new,
				"genres_best"=>$genres_best,
				"books_sale_best"=>$books_sale_best,
				"publishers"=>$publishers,
				"features_books"=>$features_books,
				"features_books_alone"=>$features_books_alone
			]);
	}
	public function detail($slug){
		$str = explode("-", $slug);
		$id = $str[count($str)-1];
		$book = DB::table("books")->where("id","=",$id)->first();
		// var_dump($book);
		$publisher = DB::table("publishers")->where("id","=",$book->publisher_id)->first();

		$author = DB::table("authors")->where("id","=",$book->author_id)->first();
		// $genres = DB::table("")->query(
		// 	"SELECT genres.id,genres.name FROM genres_books inner join genres on 
		// 		genres.id=genres_books.genre_id where genres_books.book_id =  
		// 	".$id
		// );
		$date = date("Y-m-d h:i:s",time());
		// $discount = DB::table("discounts_books")->select(["sum(value) as val"])->join("discounts","discounts.id","=","discounts_books.discount_id")->where("book_id","=",$book->id)->groupBy("book_id")->first();
		$discount = DB::table("")->query("
			SELECT sum(value) as val FROM discounts_books inner join discounts ON
				discounts.id=discounts_books.discount_id and discounts.expiry_at >='".$date."' Where book_id=".$book->id." Group by book_id LIMIT 1
			");
		$genres = DB::table("genres_books")->select(["genres.id","genres.name"])->join("genres","genres.id","=","genres_books.genre_id")->where("genres_books.book_id","=",$book->id)->get();

		$reviews = DB::table("")->query(
			"SELECT customers.username,reviews.rate,reviews.created_at,reviews.content FROM reviews inner join customers ON reviews.customer_id = customers.id Where book_id=".$book->id
		);
		$rateAVG = DB::table("")->query("
			SELECT AVG(rate) as avg_rate,count(book_id) as count_rate FROM reviews inner join books ON
			books.id=reviews.id Where book_id=".$book->id." group by reviews.book_id LIMIT 1
			");

		return $this->view("home.detail",[
			"book"=>$book,
			"publisher"=>$publisher,
			"author"=>$author,
			"discount"=>isset($discount[0])?$discount[0]:0,
			"genres"=>$genres,
			"rateAVG"=>isset($rateAVG[0])?$rateAVG[0]:null,
			"reviews"=>$reviews
		]);
	}
	public function cart(){
		if (isset($_SESSION["username"])) {
			$user = Auth::customer();
			$commune = DB::table("communes","uft8")->where("id","=",$user->commune_id)->first();
			$district = DB::table("districts","uft8")->where("id","=",$commune->district_id)->first();
			$province = DB::table("provinces","uft8")->where("id","=",$district->province_id)->first();
			$address = $commune->name." - ".$district->name." - ".$province->name;
			$provinces = DB::table("provinces","utf8")->get();
			return $this->view("home.cart",[
				"user"=>$user,
				"address"=>$address,
				"provinces"=>$provinces
			]);
		}
		return $this->view("home.login");
	}
	public function add_cart(){
		$request = new Request;
		// unset($_SESSION["cart"]);
		$book = DB::table("books")->where("id","=",$request->id)->first();
		$discount = DB::table("discounts_books")->select(["sum(value) as dis"])->join("discounts","discounts.id","=","discounts_books.discount_id")->where("book_id","=",$book->id)->groupBy("book_id")->first();
		$dis = isset($discount->dis)?$discount->dis:0;

		if (isset($_SESSION["cart"])) {
			$data = $_SESSION["cart"];
				$check = false;
			foreach ($data as $cart) {
				if ((int)$cart["id"]==(int)$request->id) {
					(int)$cart["quantity"]+= (int)$request->quantity;
					$check=true;
					break;
				}
			}

				if (!$check) {
					array_push($data,[
					"id"=>(int)$book->id,
					"name"=>$book->name,
					"image"=>$book->image,
					"quantity"=>(int)$request->quantity,
					"price_old"=>(float)$book->price,
					"price_new"=>$book->price-$book->price*$dis/100,
					"discount"=>(float)$dis
					]);
					$_SESSION["cart"] = $data;
				}else{
					// var_dump($data);
					$_SESSION["cart"] = $data;
				}
			
		}else{
			$_SESSION["cart"] = [
				[
					"id"=>$book->id,
					"name"=>$book->name,
					"image"=>$book->image,
					"quantity"=>$request->quantity,
					"price_old"=>$book->price,
					"price_new"=>$book->price-$book->price*$dis/100,
					"discount"=>(float)$dis
				],
			];
		}
		return $this->redirect($request->reference);
	}
	public function delete($id){
		$request = new Request;
		foreach ($_SESSION["cart"] as $key => $value) {
			if ((int)$value["id"]==(int)$id) {
				unset($_SESSION["cart"][$key]);
			}
		}

		return $this->redirect($request->reference);
	}
	public function save_cart(){
		$request = new Request;
		$user = Auth::customer();
		$validated = $request->validated([
			"book_id"=>"required",
			"quantity"=>"required"
		]);
		$commune = DB::table("communes","uft8")->where("id","=",$user->commune_id)->first();
			$district = DB::table("districts","uft8")->where("id","=",$commune->district_id)->first();
			$province = DB::table("provinces","uft8")->where("id","=",$district->province_id)->first();
			$provinces = DB::table("provinces","uft8")->get();
			$address = $commune->name." - ".$district->name." - ".$province->name;
		// var_dump($validated);
		if (!is_null($validated) && !empty($validated)) {

			return $this->view("home.cart",[
					"user"=>Auth::customer(),
					"errors"=>$validated,
					"address"=>$address,
					"provinces"=>$provinces
				]);
		}
		$errors = array();
		for($i=0;$i<count($request->book_id);$i++){
			$book = DB::table("books")->select(["name","quantity"])->where("id","=",$request->book_id[$i])->first();
			if ((int)$request->quantity[$i]>$book->quantity) {
				array_push($errors,$book->name."( ".$book->quantity." )"." không đủ sl ".$request->quantity[$i]." để tạo đơn hàng");
			}
			
		}
		if (count($errors)!=0) {
			
				return $this->view("home.cart",[
					"user"=>Auth::customer(),
					"errors"=>$errors,
					"address"=>$address,
					"provinces"=>$provinces
				]);
			}
			// var_dump($errors);
		$check = $request->address;
		$address = "";
		if (is_null($check)) {
			$commune = DB::table("communes","uft8")->where("id","=",$user->commune_id)->first();
			$district = DB::table("districts","uft8")->where("id","=",$commune->district_id)->first();
			$province = DB::table("provinces","uft8")->where("id","=",$district->province_id)->first();
			$address = $commune->name." - ".$district->name." - ".$province->name;
		}else{
			$commune = DB::table("communes","uft8")->where("id","=",$request->commune_id)->first();
			$district = DB::table("districts","uft8")->where("id","=",$request->district_id)->first();
			$province = DB::table("provinces","uft8")->where("id","=",$request->province_id)->first();
			$address = $commune->name." - ".$district->name." - ".$province->name;
		}
		DB::table("orders")->insert([
			"customer_id"=>Auth::customer()->id,
			"status"=>0,
			"address"=>$address,
			"created_at"=>date("Y-m-d h:i:s"),
			"updated_at"=>date("Y-m-d h:i:s")
		]);
		$order_id = DB::table("orders")->select(["id"])->orderBy("id","DESC")->first()->id;
		for($i=0;$i<count($request->book_id);$i++){
			$book = DB::table("books")->where("id","=",$request->book_id[$i])->first();
			$discount = DB::table("discounts_books")->select(["sum(value) as val"])->join("discounts","discounts.id","=","discounts_books.discount_id")->where("book_id","=",$request->book_id[$i])->groupBy("book_id")->first();
			$dis = isset($discount->val)?$discount->val:0;
			DB::table("orders_details")->insert([
				"book_id"=>$request->book_id[$i],
				"order_id"=>$order_id,
				"quantity"=>$request->quantity[$i],
				"price"=>$book->price-$book->price*$dis/100,
				"created_at"=>date("Y-m-d h:i:s",time()),
			]);	
			DB::table("books")->where("id","=",$request->book_id[$i])->update([
				"quantity"=>(int)$book->quantity-(int)$request->quantity[$i]
			]);

		}
		unset($_SESSION["cart"]);
		$details = DB::table("orders_details")->where("order_id","=",$order_id)->get();
		

		sendMail($user->email,"order","Xác nhận đơn hàng ",[
			"order_id"=>$order_id,
			"user"=>$user,
			"address"=>$address,
			"details"=>$details
		]);
		return $this->view("home.report",[
			"order_id"=>$order_id,
			"user"=>$user,
			"address"=>$address,
			"details"=>$details
		]);

	}
	public function genres($slug){
		$request = new Request;
		$page = is_null($request->page)?1:$request->page;
		$id = getID($slug);
		$genres = DB::table("genres")->get();
		$authors = DB::table("authors")->select(["authors.id","authors.name","count(books.id) as count_books"])->join("books","books.author_id","=","authors.id")->groupBy("authors.id")->get();
		// echo $authors;
		$publishers = DB::table("publishers")->select(["publishers.id","publishers.name","count(books.id) as count_books"])->join("books","books.publisher_id","=","publishers.id")->groupBy("publishers.id")->get();
		$genre = DB::table("genres")->where("id","=",$id)->first();
		$key = "Thể loại :".$genre->name;
		$count = count(DB::table("")->query("SELECT id From books Where id IN(
			SELECT book_id FROM genres_books Where genre_id=".$id."
				 )"));
		$limit = 9;
		$offset = $page==1?0:$page*$limit-$limit;
		$books = DB::table("")->query("
			SELECT * From books Where id IN(
			SELECT book_id FROM genres_books Where genre_id=".$id."
				 ) LIMIT ".$limit." OFFSET ".$offset);

		return $this->view('home.pattern',[
			"key"=>$key,
			"genres"=>$genres,
			"publishers"=>$publishers,
			"books"=>$books,
			"authors"=>$authors,
			"page"=>$page,
			"slug"=>'the-loai/'.$slug,
			"paginate"=>(int)$count/$limit
		]);
	}
	public function publishers($slug){
		$request = new Request;
		$page = is_null($request->page)?1:$request->page;
		$id = getID($slug);

		$genres = DB::table("genres")->get();

		$publisher = DB::table("publishers")->where("id","=",$id)->first();
		$key = $publisher->name;
		$authors = DB::table("authors")->select(["authors.id","authors.name","count(books.id) as count_books"])->join("books","books.author_id","=","authors.id")->groupBy("authors.id")->get();
		$publishers = DB::table("publishers")->select(["publishers.id","publishers.name","count(books.id) as count_books"])->join("books","books.publisher_id","=","publishers.id")->groupBy("publishers.id")->get();
		$limit = 9;
		$offset = $page==1?0:$page*$limit-$limit;
		$count  = count(DB::table("books")->join("publishers","publishers.id","=","books.publisher_id")->where("publishers.id","=",$id)->get());
		$books = DB::table("books")->select(["books.id","books.name","books.image","books.price","books.author_id"])->join("publishers","publishers.id","=","books.publisher_id")->where("publishers.id","=",$id)->orderBy("books.id","DESC")->limit($limit)->offset($offset)->get();
		// echo '<pre>';
		// var_dump($books);
		return $this->view('home.pattern',[
			"key"=>$key,
			"genres"=>$genres,
			"publishers"=>$publishers,
			"books"=>$books,
			"authors"=>$authors,
			"page"=>$page,
			"slug"=>'nha-xuat-ban/'.$slug,
			"paginate"=>(int)$count/$limit
		]);
	}
	public function authors($slug){
		$request = new Request;
		$page = is_null($request->page)?1:$request->page;
		$id = getID($slug);

		$genres = DB::table("genres")->get();

		$author = DB::table("authors")->where("id","=",$id)->first();
		$key = $author->name;
		$authors = DB::table("authors")->select(["authors.id","authors.name","count(books.id) as count_books"])->join("books","books.author_id","=","authors.id")->groupBy("authors.id")->get();
		$publishers = DB::table("publishers")->select(["publishers.id","publishers.name","count(books.id) as count_books"])->join("books","books.publisher_id","=","publishers.id")->groupBy("publishers.id")->get();
		$limit = 9;
		$offset = $page==1?0:$page*$limit-$limit;
		$count = count(DB::table("books")->select(["books.id","books.name","books.image","books.price","books.author_id"])->join("authors","authors.id","=","books.author_id")->where("authors.id","=",$id)->get());
		$books = DB::table("books")->select(["books.id","books.name","books.image","books.price","books.author_id"])->join("authors","authors.id","=","books.author_id")->where("authors.id","=",$id)->orderBy("books.id","DESC")->limit($limit)->offset($offset)->get();
		return $this->view('home.pattern',[
			"key"=>$key,
			"genres"=>$genres,
			"publishers"=>$publishers,
			"books"=>$books,
			"authors"=>$authors,
			"page"=>$page,
			"slug"=>'tac-gia/'.$slug,
			"paginate"=>(int)$count/$limit
		]);
	}
	public function books(){
		$request = new Request;
		$page = is_null($request->page)?1:$request->page;

		$genres = DB::table("genres")->get();

		$key = "Tất cả";
		$authors = DB::table("authors")->select(["authors.id","authors.name","count(books.id) as count_books"])->join("books","books.author_id","=","authors.id")->groupBy("authors.id")->get();
		$publishers = DB::table("publishers")->select(["publishers.id","publishers.name","count(books.id) as count_books"])->join("books","books.publisher_id","=","publishers.id")->groupBy("publishers.id")->get();
		$limit = 9;
		$offset = $page==1?0:$page*$limit-$limit;
		$count = count(DB::table("books")->select(["books.id"])->orderBy("id","DESC")->get());
		$books = DB::table("books")->orderBy("books.id","DESC")->limit($limit)->offset($offset)->get();
		return $this->view('home.pattern',[
			"key"=>$key,
			"genres"=>$genres,
			"publishers"=>$publishers,
			"books"=>$books,
			"authors"=>$authors,
			"page"=>$page,
			"slug"=>'sach-moi-nhat',
			"paginate"=>(int)$count/$limit
		]);
	}
	public function ajax(){
		$request = new Request;
		// if ($request->keyword=="") {
		// 	die(null);
		// }
		if ($request->genre =="") {

			$books = DB::table("books")->where("name","LIKE","\"%".$request->keyword."%\"")->get();
			$data = array();
			foreach ($books as $book) {
				array_push($data,[
					"name"=>$book->name,
					"id"=>$book->id,
					"slug"=>name_to_slug($book->name, $book->id)
				]);
			}
			die(json_encode($data));
		}else{
			$books = DB::table("")->query("SELECT * From books Where id IN(
			SELECT book_id FROM genres_books Where genre_id=".(int)$request->genre."
				 )");
			$data = array();
			foreach ($books as $book) {
				array_push($data,[
					"name"=>$book->name,
					"id"=>$book->id,
					"slug"=>name_to_slug($book->name, $book->id)
				]);
			}
			die(json_encode($data));
		}
	}
	public function _404(){
		return $this->view("home.404");
	}
}