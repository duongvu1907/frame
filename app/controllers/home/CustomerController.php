<?php
/**
 * 
 */
namespace app\controllers\home;

use app\core\Controller;
use app\core\Registry;
use app\core\Auth;
use app\validator\Request;
use app\database\DB;
class CustomerController extends Controller
{
	function __construct()
	{

		parent::__construct();
		$this->layout("home");
	}
	public function login(){
		if(isset($_SESSION["username"])){
			return $this->redirect(url(''));
		}
		return $this->view('home.login',[

		]);
	}
	public function do_login(){
		$request = new Request;
		$validated = $request->validated([
			"username"=>"required","password"=>"required"
		]);
		if(!is_null($validated) && !empty($validated)){
			return $this->view('home.login',[
				"errors"=>$validated
		]);
		}
		$user = DB::table("customers")->where("username","LIKE","\"".trim($request->username)."\"")->first();
		if ($user) {
			if (password_verify(trim($request->password),$user->password)) {
				$_SESSION["username"] = $user->username;
				return $this->redirect($request->reference);
			}
			return $this->view('home.login',[
				"errors"=>["Mật khẩu không chính xác"]
			]);
		}
		return $this->view('home.login',[
				"errors"=>["Tài khoản không tồn tại"]
			]);

	}
	public function register(){
		$provinces = DB::table("provinces","utf8")->get();
		return $this->view('home.register',[
			"provinces"=>$provinces
		]);
	}
	public function do_register(){
		$request = new Request;
		$validated = $request->validated([
			"name"=>"required",
			"username"=>"required|unique:customers",
			"email"=>"required|email|unique:customers",
			"password"=>"required",
			"phone"=>"required|unique:customers",
			"gender"=>"required",
			"commune_id"=>"required"
		]);
		if(!is_null($validated) && !empty($validated)){
			$provinces = DB::table("provinces","utf8")->get();
			return $this->view('home.register',[
				"errors"=>$validated,
				"provinces"=>$provinces
			]);

		}

		DB::table("customers")->insert([
			"name"=>$request->name,
			"username"=>$request->username,
			"email"=>$request->email,
			"password"=>password_hash($request->password, PASSWORD_DEFAULT),
			"phone"=>$request->phone,
			"gender"=>$request->gender,
			"commune_id"=>$request->commune_id,
			"created_at"=>date("Y-m-d h:i:s",time()),
			"updated_at"=>date("Y-m-d h:i:s",time()),
		]);
		return $this->redirect(url("dang-nhap"));
	}
	public function forget(){
		return $this->view("home.forget_password");
	}
	public function send_forget(){
		$request = new Request;
		$validated = $request->validated([
			"email"=>"required|email"
		]);
		if(!is_null($validated) && !empty($validated)){
			// echo 'string';
			return $this->view('home.forget_password',[
				"errors"=>$validated
			]);
		}
		$user = DB::table("customers")->where("email","LIKE","\"".$request->email."\"")->first();
		$str = str_random(8);
		if ($user && sendMail($request->email,"forget_password",["token"=>$str,"username"=>$user->username]) ) {
			DB::table("customers")->where("email","LIKE","\"".$request->email."\"")->update([
				"password"=>password_hash($str, PASSWORD_DEFAULT)
			]);
			return $this->redirect(url('dang-nhap'));
		}
		return $this->view('home.forget_password',[
				"errors"=>[
					"email không tồn tại",
				],
		]);
	}
	public function logout(){
		if (isset($_SESSION["username"])) {
			unset($_SESSION["username"]);
		}
		return $this->redirect(url('dang-nhap'));
	}
	public function profile(){
		$user = Auth::customer();
		$orders = DB::table("orders")->where("customer_id","=",$user->id)->orderBy('id',"DESC")->get();

		$commune = DB::table("communes","uft8")->where("id","=",$user->commune_id)->first();
		$district = DB::table("districts","uft8")->where("id","=",$commune->district_id)->first();
		$province = DB::table("provinces","uft8")->where("id","=",$district->province_id)->first();

		$provinces = DB::table("provinces","uft8")->get();
		$districts = DB::table("districts","uft8")->where("province_id","=",$province->id)->get();
		$communes = DB::table("communes","uft8")->where("district_id","=",$district->id)->get();
		// var_dump($order_id);
		return $this->view('home.profile',[
			"user"=>$user,
			"orders"=>$orders,
			"commune_id"=>$commune->id,
			"district_id"=>$district->id,
			"province_id"=>$province->id,
			"communes"=>$communes,
			"districts"=>$districts,
			"provinces"=>$provinces
		]);
	}
	public function comment(){
		$request = new Request;
		$id = Auth::customer()->id;
		$validated = $request->validated([
			"content"=>"required",
			"rate"=>"required"
		]);
		DB::table("reviews")->insert([
			"book_id"=>$request->book_id,
			"customer_id"=>$id,
			"rate"=>(int)$request->rate,
			"content"=>$request->content,
			"created_at"=>date("Y-m-d h:i:s",time()),
			"updated_at"=>date("Y-m-d h:i:s",time())
		]);
		return $this->redirect($request->reference);
	}
	public function edit(){
		$request = new Request;
		$validated = $request->validated([
			"name"=>"required",
			"phone"=>"required",
			"gender"=>"required",
			"commune_id"=>"required"
		]);
		if(!is_null($validated) && !empty($validated)){
			return $this->redirect($request->reference);
		}
		if (!is_null($request->check_pass)) {
			if (password_verify($request->password,Auth::customer()->password)) {
					if ($request->new_password) {
						DB::table("customers")->where("id","=",Auth::customer()->id)->update([
							"password"=>password_hash($request->new_password, PASSWORD_DEFAULT)	
						]);
					}
				}
				// return;
			return $this->redirect($request->reference);
		}
		DB::table("customers")->where("id","=",(int)Auth::customer()->id)->update([
			"name"=>$request->name,
			"phone"=>$request->phone,
			"gender"=>$request->gender,
			"commune_id"=>(int)$request->commune_id
		]);
		return $this->redirect($request->reference);
	}

	public function get_communes_ajax(){
		$request = new Request;
		if (isset($request->commune_id)) {
			die("");
		}
		$communes = DB::table("communes","utf8")->where("district_id","=",$request->id)->get();
		die(json_encode($communes));
	}
	public function get_districts_ajax(){
		$request = new Request;
		if (isset($request->district_id)) {
			die("");
		}
		$districts = DB::table("districts","utf8")->where("province_id","=",$request->id)->get();
		die(json_encode($districts));
	}
}