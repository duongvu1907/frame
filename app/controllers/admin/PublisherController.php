<?php  
/**
 * summary
 */
namespace app\controllers\admin;

use app\core\Controller;
use app\core\Registry;
use app\validator\Request;
use app\database\DB;
use app\core\Auth;
use app\core\File;
class PublisherController extends Controller
{
    
    public function __construct()
    {
		parent::__construct();
       $this->layout("admin");
    }
    public function index(){
        $publishers = DB::table("publishers")->get();
    	return $this->view("admin.publisher",[
            "publishers"=>$publishers
        ]);
    }
    public function do_add(){
        $request = new Request;
        $name = "";
        $validated = $request->validated([
            "name"=>"required",
            "address"=>"required"
        ]);
        echo 'string';
        if (!is_null($validated) && !empty($validated)) {
            return $this->redirect($request->reference);
        }
        // var_dump($request->image);
        if (!empty($request->image["name"])) {
            $name = File::move($request->image,"upload/publishers/");
        }
        DB::table("publishers")->insert([
            "name"=>$request->name,
            "address"=>$request->address,
            "image"=>$name,
            "created_at"=>date("Y-m-d h:i:s",time()),
            "updated_at"=>date("Y-m-d h:i:s",time())
        ]);

        return $this->redirect($request->reference);
    }
    public function ajax(){
        $request = new Request;
        $publisher = DB::table("publishers")->where("id","=",$request->id)->first();
        die(json_encode($publisher));
    }
    public function do_edit(){
        $request = new Request;
        $publisher = DB::table("publishers")->where("id","=",$request->id)->first();

        DB::table("publishers")->where("id","=",$request->id)->update([
            "name"=>$request->name,
            "address"=>$request->address,
        ]);
        // var_dump(empty($request->image["name"]));
        if (!empty($request->image["name"])) {
            $name = File::move($request->image,"upload/publishers/");
            if ($name) {
                File::delete("upload/publishers/".$publisher->image);
                DB::table("publishers")->where("id","=",$request->id)->update([
                    "image"=>$name
                ]);
            }
        }
    return $this->redirect(url('admin/publishers'));
    }
    public function delete($id){
        $publisher = DB::table("publishers")->where("id","=",$id)->first();
        File::delete("upload/publishers/".$publisher->image);

        $slider = DB::table("sliders")->where("filter","LIKE","\"publishers\"")->where("foreign_key","=",$id)->first();
        if ($slider) {
            File::delete("upload/sliders".$slider->image);
            DB::table("sliders")->where("foreign_key","=",$id)->delete();
        }

        DB::table("publishers")->where("id","=",$id)->delete();
        return $this->redirect(url('admin/publishers'));
    }
}