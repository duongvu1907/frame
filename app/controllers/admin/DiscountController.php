<?php  
/**
 * summary
 */
namespace app\controllers\admin;
use app\core\Controller;
use app\database\DB;
use app\core\File;
use app\validator\Request;
class DiscountController extends Controller
{   
    public function __construct()
    {
    	$this->layout("admin");
        parent::__construct();
    }
    public function index(){
    	$date = date("Y-m-d h:i:s");
    	$discounts_available = DB::table("discounts")->where("expiry_at",">=","\"$date\"")->get();
    	$discounts_expiried = DB::table("discounts")->where("expiry_at","<","\"$date\"")->get();
    	return $this->view("admin.discount",[
    		"discounts_available"=>$discounts_available,
    		"discounts_expiried"=>$discounts_expiried
    	]);
    }
    public function add(){
    	$request = new Request;
    	$validated = $request->validated([
    		"name"=>"required",
    		"value"=>"required",
    		"expiry_at"=>"required",
    		"start_at"=>"required",
    	]);
    	if (str_to_datetime($request->expiry_at)<=str_to_datetime($request->start_at)) {
    		array_push($validated,"Error input datetime");
    	}
    	if (!is_null($validated) && !empty($validated)) {

    		return $this->redirect(url("admin/discounts"));
    	}
    	$name = "";
    	if (!empty($request->image["name"])) {
    		$name = File::move($request->image,"upload/discounts/");
    	}
    	DB::table("discounts")->insert([
    		"name"=>$request->name,
    		"value"=>(float)$request->value,
    		"expiry_at"=>$request->expiry_at,
    		"start_at"=>$request->start_at,
    		"image"=>$name,
    		"created_at"=>date("Y-m-d h:i:s",time())
    	]);
    	return $this->redirect(url('admin/discounts'));
    }
    public function mixed(){
    	$discounts = DB::table("discounts")->where("expiry_at",">=","\".date('Y-m-d h:i:s',time())\"")->get();
    	$books = DB::table("books")->get();
    	$customers = DB::table("customers")->get();
    	return $this->view("admin.mixed",[
    		"discounts"=>$discounts,
    		"books"=>$books,
    		"customers"=>$customers
    	]);
    }
    public function ajax(){
    	$request = new Request;
    	$validated = $request->validated([
    		"id"=>"required"
    	]);
    	if (!is_null($validated) && !empty($validated)) {

    		die();
    	}
    	$discount = DB::table("discounts")->where("id","=",$request->id)->first();
    	die(json_encode($discount));
    }
    public function ajax_name(){
        $request = new Request;
        $discount = DB::table("discounts")->where("name","LIKE","\"%".$request->keyword."%\"")->get();
        die(json_encode($discount));
    }
    public function edit(){
    	$request = new Request;
    	$validated = $request->validated([
    		"id"=>"required",
    		"name"=>"required",
    		"value"=>"required",
    	]);
    	if (!is_null($validated) && !empty($validated)) {
    		return $this->redirect(url("admin/discounts"));
    	}
    	$discount = DB::table("discounts")->where("id","=",$request->id)->first();
    	$expiry_at = $request->expiry_at==""?$discount->expiry_at:$request->expiry_at;
    	$start_at = $request->start_at==""?$discount->start_at:$request->start_at;
    	// echo $request->id;
    	if (!empty($request->image["name"])) {
    		$name = File::move($request->image,"upload/discounts/");
    		DB::table("discounts")->where("id","=",$request->id)->update([
    			"image"=>$name
    		]);
    	}
    	// echo '<pre>';
    	// var_dump([
    	// 		"name"=>$request->name,
    	// 		"value"=>(float)$request->value,
    	// 		"expiry_at"=>$expiry_at,
    	// 		"start_at"=>$start_at
    	// 	]);
		DB::table("discounts")->where("id","=",$request->id)->update([
    			"name"=>$request->name,
    			"value"=>(float)$request->value,
    			"expiry_at"=>$expiry_at,
    			"start_at"=>$start_at
    		]);   
    	return $this->redirect(url('admin/discounts')); 	

    }
    public function do_mixed(){
    	$request = new Request;
    	$validated = $request->validated([
    		"filter"=>"required",
    		"keyword"=>"required"
    	]);

    	// DB::table("")
    }
    public function discounts_books(){
    	$request = new Request;
    	$validated = $request->validated([
    		"book_id"=>"required",
    		"discount_id"=>"required"
    	]);
    	if (!is_null($validated) && !empty($validated)) {
    		return $this->redirect(url("admin/discounts/mixed"));
    	}
    	DB::table("discounts_books")->insert([
    		"book_id"=>(int)$request->book_id,
    		"discount_id"=>(int)$request->discount_id,
    		"created_at"=>date("Y-m-d h:i:s",time()),
    		"updated_at"=>date("Y-m-d h:i:s",time()),
    	]);
    	return $this->redirect(url("admin/discounts/mixed"));
    }
    public function discounts_customers(){
    	$request = new Request;
    	$validated = $request->validated([
    		"customers_id"=>"required",
    		"discount_id"=>"required"
    	]);
    	if (!is_null($validated) && !empty($validated)) {
    		return $this->redirect(url("admin/discounts/mixed"));
    	}
    	DB::table("discounts_customers")->insert([
    		"customers_id"=>$request->customers_id,
    		"discount_id"=>$request->discount_id,
    		"created_at"=>date("Y-m-d h:i:s",time()),
    		"updated_at"=>date("Y-m-d h:i:s",time()),
    	]);
    	return $this->redirect(url("admin/discounts/mixed"));
    }
    public function delete($id){
    	$discount = DB::table("discounts")->where("id","=",$id)->first();
    	if ($discount->image!="") {
            File::delete("upload/discounts/".$discount->image);
        }
        DB::table("discounts_books")->where("discount_id","=",$discount->id)->delete();
    	DB::table("discounts")->where("id","=",$id)->delete();
    	return $this->redirect(url('admin/discounts'));
    }
    public function statistics(){
    	$request = new Request;
    	$validated = $request->validated([
    		"filter"=>"required",
    		"id"=>"required"
    	]);
    	if (!is_null($validated) && !empty($validated)) {
    		die();
    	}
    	$table = trim($request->filter);
    	$id = str_replace("s","",$table);
    	$discounts = DB::table("discounts_".$table)->where($id."_id","=",$request->id)->get();
    	$data = DB::table($table)->where("id","=",$request->id)->first();
    	$response = array();
    	foreach ($discounts as $discount) {
    		$status = $this->check_expiry((int)$discount->discount_id)==true?1:0;
    		$discount_name = DB::table("discounts")->where("id","=",$discount->discount_id)->first();
    		array_push($response,[
    			"id"=>$discount->id,
    			"name"=>$data->name,
    			"discount_name"=>$discount_name->name,
    			"discount_value"=>$discount_name->value,
    			"status"=>$status,
    			"filter"=>$request->filter
    		]);
    	}
    	die(json_encode($response));
    }
    public function check_expiry($id){
    	$discount = DB::table("discounts")->where("id","=",$id)->first();
    	$date = date("Y-m-d h:i:s",time());
    	if ($date<=$discount->expiry_at) {
    		return true;
    	}
    	return false;
    }
    public function delete_mixed($filter,$id){
    	$request = new Request;
    	$url = url("admin/discounts/mixed");
    	if ($url == $request->reference) {
    		// echo 'string';
    		// echo "discousts_".trim($filter);
    		DB::table("discounts_".trim($filter))->where("id","=",$id)->delete();
    	}
    		return $this->redirect(url('admin/discounts/mixed'));
    }
}