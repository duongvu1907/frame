<?php  
/**
 * summary
 */
namespace app\controllers\admin;

use app\core\Controller;
use app\core\Registry;
use app\validator\Request;
use app\core\Auth;
use app\database\DB;
use app\core\File;
class ProductController extends Controller
{
    
    public function __construct()
    {
		parent::__construct();
       $this->layout("admin");
    }
    public function index(){
        $books = DB::table("books")->orderBy("id","DESC")->get();
    	return $this->view("admin.product",[
            "books"=>$books,
        ]);
    }
    public function add(){
        $genres = DB::table("genres")->get();
        $authors = DB::table("authors")->get();
        $publishers = DB::table("publishers")->get();
        return $this->view("admin.add_product",[
            "genres"=>$genres,
            "authors"=>$authors,
            "publishers"=>$publishers
        ]);
    }
    public function do_add(){
        $request = new Request;
        // $db = new DB("books");
        // echo '<pre>';
        // var_dump($request);
        // echo $db->lastInsertedId();
        // $public = isset($request->public) && $request->public=="on"?1:0;
        if (!empty($request->image["name"])) {
            $name = File::move($request->image,"upload/books/");
        }
         DB::table("books")->insert([
            "name"=>$request->name,
            "description"=>htmlentities($request->description),
            "price"=>(float)$request->price,
            "author_id"=>(int)$request->author_id,
            "quantity"=>(int)$request->quantity,
            "publisher_id"=>(int)$request->publisher_id,
            "image"=>$name,
            "created_at"=>date("Y-m-d h:i:s",time()),
            "updated_at"=>date("Y-m-d h:i:s",time())
        ]);
        $id = DB::table("books")->orderBy("id","DESC")->first()->id;
        foreach ($request->genres_id as $genre_id) {
            DB::table("genres_books")->insert([
                "genre_id"=>$genre_id,
                "book_id"=>$id
            ]);
        }

        return $this->redirect(url("admin/product"));

    }
    public function edit($id){
        $book = DB::table("books")->where("id","=",$id)->first();
        $book_genre = DB::table("genres_books")->where("book_id","=",$book->id)->get();
        $genres = DB::table("genres")->get();
        $authors = DB::table("authors")->get();
        $publishers = DB::table("publishers")->get();
        // var_dump($book_genre);
        return $this->view("admin.edit_product",[
            "book"=>$book,
             "genres"=>$genres,
            "authors"=>$authors,
            "publishers"=>$publishers,
            "book_genre"=>$book_genre
        ]);
    }
    public function do_edit($id){
        $request = new Request;
        // $public = $request->public=="on"?1:0;
        $book = DB::table("books")->where("id","=",$id)->first();
        // echo $public;
        if (!empty($request->image["name"])) {
            File::delete("upload/books/".$book->image);
            $name = File::move($request->image,"upload/books/");
            DB::table("books")->where("id","=",$id)->update([
                "image"=>$name
            ]);
        }
        DB::table("books")->where("id","=",$id)->update([
             "name"=>$request->name,
            "description"=>htmlentities($request->description),
            "price"=>(float)$request->price,
            "author_id"=>(int)$request->author_id,
            "quantity"=>(int)$request->quantity,
            "publisher_id"=>(int)$request->publisher_id,
            "updated_at"=>date("Y-m-d h:i:s",time())
        ]);
        DB::table("genres_books")->where("book_id","=",$id)->delete();
        foreach ($request->genres_id as $genre_id) {
            DB::table("genres_books")->insert([
                "genre_id"=>$genre_id,
                "book_id"=>$id
            ]);
        }
        return $this->redirect(url("admin/product"));
    }
    public function delete($id){
        $book = DB::table("books")->where("id","=",$id)->first();
        File::delete("upload/books/".$book->image);

        // Delete orders 
        DB::table("orders_details")->where("book_id","=",$book->id)->delete();

        DB::table("genres_books")->where("book_id","=",$id)->delete();
        DB::table("discounts_books")->where("book_id","=",$id)->delete();
        $fb = DB::table("features_books")->where("book_id","=",$id)->first();
        $slider = DB::table("sliders")->where("filter","LIKE","\"books\"")->where("foreign_key","=",$id)->first();
        if($fb){
            File::delete("upload/features_books/".$fb->image);
            DB::table("features_books")->where("book_id","=",$id)->delete();
        }
        if ($slider) {
            File::delete("upload/sliders".$slider->image);
            DB::table("sliders")->where("foreign_key","=",$id)->delete();
        }
        DB::table("reviews")->where("book_id","=",$id)->delete();

        DB::table("books")->where("id","=",$id)->delete();
        return $this->redirect(url("admin/product"));
    }
    public function ajax(){
        $request = new Request;
        $book = DB::table("books")->where("name","LIKE","\"%".$request->keyword."%\"")->get();
        die(json_encode($book));
    }

}