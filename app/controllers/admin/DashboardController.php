<?php  
/**
 * summary
 */
namespace app\controllers\admin;

use app\core\Controller;
use app\database\DB;
use app\core\Registry;
use app\validator\Request;
use app\core\Auth;
class DashboardController extends Controller
{
    
    public function __construct()
    {
		parent::__construct();
       $this->layout("admin");
    }
    public function index(){
    	$user = Auth::user();
    	// var_dump(Registry::getInstance()->auth);
        $orders_new = DB::table("orders")->where("status","=",0)->get();
        $orders_old= DB::table("orders")->where("status","=",1)->orderBy("id","DESC")->get();

        $books_sale_best = DB::table("")->query(
            "SELECT books.id,books.author_id,books.image,books.name,books.price,count(book_id) as orders_number FROM books inner join orders_details ON orders_details.book_id = books.id group by books.id order by orders_number DESC LIMIT 5"
        ); 
        $genres_best = DB::table("")->query(
            "SELECT * FROM genres where id IN (SELECT genre_id from genres_books where book_id IN (SELECT books.id  FROM books inner join orders_details ON orders_details.book_id = books.id group by books.id order by count(book_id) DESC) GROUP BY genre_id ) LIMIT 5"
        );
        $customer = DB::table("customers")->get();

    	return $this->view("admin.dashboard",[
    		"user"=>$user,
            "orders_new"=>$orders_new,
            "orders_old"=>$orders_old,
            "books_sale_best"=>$books_sale_best,
            "genres_best"=>$genres_best,
            "customers"=>$customer
    	]);
    }
    public function active_order($id){
        $order = DB::table("orders")->where("id","=",$id)->first();
        $customer_id = DB::table("orders")->where("id","=",$id)->first()->customer_id;
        $user = DB::table("customers")->where("id","=",$customer_id)->first();
        $orders_details = DB::table("orders_details")->where("order_id","=",$id)->get();
        return $this->view("admin.active_order",[
            "orders_details"=>$orders_details,
            "order"=>$order,
            "customer_id"=>$customer_id,
            "user"=>$user
        ]);
    }
    public function save_order(){
        $request = new Request;
        DB::table("orders")->where("id","=",$request->order_id)->update([
            "status"=>1,
            "amount"=>$request->amount
        ]);
        return $this->redirect(url('admin/dashboard'));
    }
    public function delete_customer($id){
        DB::table("reviews")->where("customer_id","=",$id)->delete();
        $order = DB::table("orders")->where("customer_id","=",$id)->get();
        // var_dump($order);
        foreach ($order as $o) {
            DB::table("orders_details")->where("order_id","=",$o->id)->delete();
        }
        DB::table("orders")->where("customer_id","=",$id)->delete();
        DB::table("customers")->where("id","=",$id)->delete();
        return $this->redirect(url('admin/dashboard'));

    }
}