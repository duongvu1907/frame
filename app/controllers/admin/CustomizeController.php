<?php

/**
 * summary
 */
namespace app\controllers\admin;
use app\core\Controller;
use app\core\File;
use app\database\DB;
use app\validator\Request;
class CustomizeController extends Controller
{
    /**
     * summary
     */
    public function __construct()
    {
    	$this->layout("admin");
    	parent::__construct();   
    }
    public function index(){
    	$sliders = DB::table("sliders")->get();
        $features_books = DB::table("features_books")->get();
    	return $this->view("admin.customize",[
    		"sliders"=>$sliders,
            "features_books"=>$features_books
    	]);
    }
    public function add_slider(){
    	$request = new Request;
    	$validated = $request->validated([
    		"foreign_key"=>"required",
    		"title"=>"required",
            "description"=>"required",
            "image"=>"required",
            "filter"=>"required"
    	]);
    	if (!is_null($validated) && !empty($validated)) {
    		return $this->redirect(url("admin/customize"));
    	}
    	$id  = $request->foreign_key;
    	
    	$description = $request->description;
    	$image = File::move($request->image,"upload/sliders/");
        // echo '<pre>';
        // var_dump();
    	DB::table("sliders")->insert([
    		"foreign_key"=>$id,
            "filter"=>$request->filter,
    		"description"=>$description,
    		"image"=>$image,
    		"title"=>$request->title
    	]);
    	return $this->redirect(url("admin/customize"));
    }
    public function ajax(){
    	$request = new Request;
    	$slider = DB::table("sliders")->where("id","=",$request->slider_id)->first();
    	$filter = DB::table($slider->filter)->where("id","=",$slider->foreign_key)->first();
    	die(json_encode(["filter"=>$filter,"slider"=>$slider]));
    }
    public function edit(){
        $request = new Request;
        $validated = $request->validated([
            "id"=>"required",
            "title"=>"required",
            "description"=>"required"
        ]);
        if (!is_null($validated) && !empty($validated)) {
            return $this->redirect(url("admin/customize"));
        }
        $slider = DB::table("sliders")->where("id","=",$request->id)->first();
        if (!empty($request->image["name"])) {
            // echo 'string';
            File::delete("upload/sliders/".$slider->image);
            $image = File::move($request->image,"upload/sliders/");
            DB::table("sliders")->where("id","=",$request->id)->update([
                "image"=>$image
            ]);
        }
        DB::table("sliders")->where("id","=",$request->id)->update([
                "description"=>$request->description,
                "title"=>$request->title,
            ]);
        return $this->redirect($request->reference);

    }
    function delete($id){
        $request= new Request;
        $slider = DB::table("sliders")->where("id","=",$id)->first();
        File::delete("upload/sliders/".$slider->image);
        DB::table("sliders")->where("id","=",$id)->delete();
        return $this->redirect($request->reference);
    }

    public function add_features_books(){
        $request = new Request;
        $validated = $request->validated([
            "book_id"=>"required",
            "title"=>"required",
            "image"=>"required"
        ]);
        if (!is_null($validated) && !empty($validated)) {
            return $this->redirect(url("admin/customize"));
        }
        $image = File::move($request->image,"upload/features_books/");
        DB::table("features_books")->insert([
            "book_id"=>$request->book_id,
            "title"=>$request->title,
            "image"=>$image
        ]);
        return $this->redirect($request->reference);
    }
    public function delete_fb($id){
        $request= new Request;
        $fb = DB::table("features_books")->where("id","=",$id)->first();
        File::delete("upload/features_books/".$fb->image);
        DB::table("features_books")->where("id","=",$id)->delete();
        return $this->redirect($request->reference);
    }
}