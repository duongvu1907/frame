<?php  
/**
 * summary
 */
namespace app\controllers\admin;

use app\core\Controller;
use app\core\Registry;
use app\validator\Request;
use app\database\DB;
use app\core\Auth;
use app\core\File;
class GenreController extends Controller
{
    
    public function __construct()
    {
		parent::__construct();
       $this->layout("admin");
    }
    public function index(){
        $genres = DB::table("genres")->get();
    	return $this->view("admin.genre",[
            "genres"=>$genres
        ]);
    }
    public function do_add(){
        $request = new Request;
        $name = "";
        $validated = $request->validated([
            "name"=>"required",
        ]);
        if (!is_null($validated) && !empty($validated)) {
            return $this->redirect($request->reference);
        }
        
        DB::table("genres")->insert([
            "name"=>$request->name,
        ]);
        return $this->redirect($request->reference);
    }
    public function ajax(){
        $request = new Request;
        $genre = DB::table("genres")->where("id","=",$request->id)->first();
        die(json_encode($genre));
    }
    public function do_edit(){
        $request = new Request;
        DB::table("genres")->where("id","=",$request->id)->update([
            "name"=>$request->name,
        ]);
        // var_dump(empty($request->image["name"]));
    return $this->redirect(url('admin/genres'));
    }
    public function delete($id){
        // var_dump($id);
        $genre = DB::table("genres")->where("id","=",$id)->first();
        DB::table("genres_books")->where("genre_id","=",$genre->id)->delete();
        $slider = DB::table("sliders")->where("filter","LIKE","\"genres\"")->where("foreign_key","=",$id)->first();
        if ($slider) {
            File::delete("upload/sliders".$slider->image);
            DB::table("sliders")->where("foreign_key","=",$id)->delete();
        }
        DB::table("genres")->where("id","=",(int)$id)->delete();
         return $this->redirect(url('admin/genres'));
    }
}