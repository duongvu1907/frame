<?php  
/**
 * summary
 */
namespace app\controllers\admin;

use app\core\Controller;
use app\core\Registry;
use app\validator\Request;
use app\database\DB;
use app\core\Auth;
use app\core\File;
class AuthorController extends Controller
{
    
    public function __construct()
    {
		parent::__construct();
       $this->layout("admin");
    }
    public function index(){
        $authors = DB::table("authors")->get();
    	return $this->view("admin.author",[
            "authors"=>$authors
        ]);
    }
    public function do_add(){
        $request = new Request;
        $name = "";
        $validated = $request->validated([
            "name"=>"required",
            "stage"=>"required",
            "job"=>"required",
            "address"=>"required"
        ]);
        if (!is_null($validated) && !empty($validated)) {
            return $this->redirect($request->reference);
        }
        if (!empty($request->image["name"])) {
            $name = File::move($request->image,"upload/authors/");
        }
        DB::table("authors")->insert([
            "name"=>$request->name,
            "stage"=>$request->stage,
            "job"=>$request->job,
            "address"=>$request->address,
            "image"=>$name
        ]);
        return $this->redirect($request->reference);
    }
    public function ajax(){
        $request = new Request;
        $author = DB::table("authors")->where("id","=",$request->id)->first();
        die(json_encode($author));
    }
    public function do_edit(){
        $request = new Request;
        $author = DB::table("authors")->where("id","=",$request->id)->first();

        DB::table("authors")->where("id","=",$request->id)->update([
            "name"=>$request->name,
            "stage"=>$request->stage,
            "job"=>$request->job,
            "address"=>$request->address,
        ]);
        // var_dump(empty($request->image["name"]));
        if (!empty($request->image["name"])) {
            $name = File::move($request->image,"upload/authors/");
            if ($name) {
                File::delete("upload/authors/".$author->image);
                DB::table("authors")->where("id","=",$request->id)->update([
                    "image"=>$name
                ]);
            }
        }
    return $this->redirect(url('admin/authors'));
    }
    public function delete($id){
        $author = DB::table("authors")->where("id","=",$id)->first();
        if ($author->image!="") {
            File::delete("upload/authors/".$author->image);
        }

        $book = DB::table("books")->where("author_id","=",$id)->first();

        if ($book) {
            $authors = DB::table("authors")->get();
            return $this->view("admin.author",[
                "authors"=>$authors,
                "errors"=>["Vui long xoa sach truoc"]
            ]);
        }

        $slider = DB::table("sliders")->where("filter","LIKE","\"authors\"")->where("foreign_key","=",$id)->first();

        if ($slider) {
            File::delete("upload/sliders/".$slider->image);
            DB::table("sliders")->where("foreign_key","=",$id)->delete();
        }

        DB::table("authors")->where("id","=",$id)->delete();
        return $this->redirect(url('admin/authors'));
    }
}