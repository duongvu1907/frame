<?php /**
 * summary
 */
namespace app\database;
use \PDO;
use \PDOException;
use app\core\AppException;
use app\core\Registry;
class MysqlConnection
{
	protected $pdo;
    public function __construct($charset=null)
    {
    	$config = Registry::getInstance()->config["database"];
        $this->open($config,$charset);
    }
    public function __destruct(){
    	$this->close();
    }
    public function open($config,$charset){
 		try {
            $attr = array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
              );
            if (!is_null($charset)) {
                $attr = [
                        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
                      ];
            }
 			$this->pdo = new PDO(
	 			$this->getConnectionString($config),
	 			$config["username"],
	 			$config["password"],
                $attr
                
 			);
 		
 		} catch (PDOException $e) {
 			throw new AppException($e->getMessage(),404);
 		}
    }
    public function close(){
    	$this->pdo = null;
    }
    public function getConnectionString($config){
    	return "mysql:dbname=".$config["dbname"].";host=".$config["hostname"];
    }
    public function execute($sql){
    	return $this->pdo->exec($sql);
    }
    public function query($sql,$params=[]){
    	if (!empty($params)) {
    		$sth = $this->pdo->prepare($sql);
    		$sth = $this->pdo->exec($params);
    		return $sth;
    	}
    	return $this->pdo->query($sql);
    }
    public function charset(){
        $this->pdo->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'utf8'");
    }

} 