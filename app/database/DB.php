<?php 
namespace app\database;

use \PDO;
use \PDOException;
use app\core\AppException;
use app\database\MysqlConnection;
/**
 * summary
 */
class DB
{
	private $fetchType = PDO::FETCH_OBJ;
	protected $pdo;
    private $columns;
    private $from;
    private $distinct = false;
    private $wheres;
    private $groups;
    private $havings;
    private $orders;
    private $limits;
    private $offsets;
    private $joins;
    private $inserts;
    public function __construct($tableName="",$charset)
    {
    	$this->pdo = new MysqlConnection($charset);
        $this->from = $tableName;
    }
    public static function table($tableName,$charset=null){
    	return new self($tableName,$charset);
    }
    public function select($column){
    	$this->columns = is_array($column)?$column:func_get_args($column);
    	return $this;
    }
    public function distinct(){
    	$this->distinct = true;
    	return $this;
    }
    public function join($table,$first,$operator,$second,$type = "inner"){
    	$this->joins[]=[$table,$first,$operator,$second,$type];
    	return $this;
    }
    public function leftJoin($table,$first,$operator,$second){
    	$this->joins[]=[$table,$first,$operator,$second,"left"];
    	return $this;
    }
    public function rightJoin($table,$first,$operator,$second){
    	$this->joins[]=[$table,$first,$operator,$second,"right"];
    	return $this;
    }
    public function where($column,$operator,$value,$boolean="and"){
    	$this->wheres[] = [$column,$operator,$value,$boolean];
    	return $this;
    }
    public function orWhere($column,$operator,$value,$boolean='or'){
    	$this->wheres[] = [$column,$operator,$value,$boolean];
    	return $this;
    }
    public function groupBy($column){
    	$this->groups = is_array($column)?$column:func_get_args($column);

    	return $this;
    }
    public function having($column,$operator,$value,$boolean="and"){
    	$this->havings[] = [$column,$operator,$value,$boolean];
    	return $this;
    }
    public function orHaving($column,$operator,$value,$boolean='or'){
    	$this->havings[] = [$column,$operator,$value,$boolean];
    	return $this;
    }
    public function orderBy($column,$direction="ASC"){
    	$this->orders[] = [$column,$direction];
    	return $this;
    }
    public function limit($limit){
    	$this->limits = $limit;
    	return $this;
    }
    public function offset($offset){
    	$this->offsets = $offset;
    	return $this;
    }
    public function insert($columns){
    	$sql = "INSERT INTO ".$this->from."(";
    	$columns = is_array($columns)?$columns:func_get_args($columns);
        $i=0;
    	foreach ($columns as $key => $value) {
    		if (count($columns)==$i+1) {
    			$sql.=$key.") ";
    		}else{
    			$sql.=$key.",";
    		}
    		
            $i++;
    	}
    	$sql.= " VALUES( ";
        $i=0;
    	foreach ($columns as $key => $value) {
    		if (count($columns)==$i+1) {
    			$value = is_string($value)?"\"".$value."\"":$value;
    			$sql.=$value.") ";
    		}else{
    			$value = is_string($value)?"\"".$value."\"":$value;
    			$sql.=$value.",";
    		}
    		$i++;
    	}
        // return $sql;
    	$this->pdo->execute($sql);
    }
    public function update($columns){
    	$sql = "UPDATE ".$this->from." SET ";
    	foreach ($columns as $key => $value) {
    		if (end($columns)==$value) {
    			$value = is_string($value)?"\"".$value."\"":$value;
    			$sql.=" ".$key."=".$value;	
    		}else{
    			$value = is_string($value)?"\"".$value."\"":$value;
    			$sql.=" ".$key."=".$value.",";	
    		}
    		
    	}
    	if (isset($this->wheres) && is_array($this->wheres)) {
    		$sql.=" WHERE";
    		foreach ($this->wheres as $wk => $where) {
    			$sql.=" $where[0] $where[1] $where[2] ";
    			if ($wk<count($this->wheres)-1) {
    				$sql.=" ".strtoupper($where[3]);
    			}

    		}
    	}
    	$this->pdo->execute($sql);
    }
    public function get(){
    	$sql = $this->getCompileQuery();
    	if (isset($this->limits) && !empty($this->limits)) {
    		$sql.=" LIMIT ".$this->limits;
    	}
    	if (isset($this->offsets) && !empty($this->offsets)) {
    		$sql.=" OFFSET ".$this->offsets;
    	}
    	try {
            
            // return $sql;

    		return $this->pdo->query($sql)->fetchAll($this->fetchType);
    	} catch (PDOException $e) {
    		throw new AppException($e->getMessage());
    	}
    }
    public function first(){
    	try {
    		$sql = $this->getCompileQuery();
	    	$sql.=" LIMIT 1";
            // return $sql;
	    	return $this->pdo->query($sql)->fetch($this->fetchType);
    	} catch (PDOException $e) {
    		throw new AppException($e->getMessage());
    	}
    	
    }
    public function getCompileQuery(){
    	if (!isset($this->from)||empty($this->from)) {
    		return false;	
    	}
    	$sql = $this->distinct?'SELECT DISTINCT ':'SELECT ';
    	if (isset($this->columns) && is_array($this->columns)) {
    		$sql .= implode(' , ',$this->columns);
    	}else{
    		$sql.=" * ";
    	}
    	$sql.= ' FROM '.$this->from;
    	if (isset($this->joins) && is_array($this->joins)) {
    		foreach ($this->joins as $join) {
    			switch (strtolower($join[4])) {
    				case "inner":
    					$sql.= ' INNER JOIN ';
    					break;
    				case "left":
    					$sql.= ' LEFT JOIN ';
    					break;
    				case "right":
    					$sql.= ' RIGHT JOIN ';
    					break;
    				
    			}
    			$sql.= "$join[0] ON $join[1] $join[2] $join[3]";
    		}
    	}
    	if (isset($this->wheres) && is_array($this->wheres)) {
    		$sql.=" WHERE";
    		foreach ($this->wheres as $wk => $where) {
    			$sql.=" $where[0] $where[1] $where[2] ";
    			if ($wk<count($this->wheres)-1) {
    				$sql.=" ".strtoupper($where[3]);
    			}

    		}
    	}
    	if (isset($this->groups) && is_array($this->groups)) {
    		$sql.=" GROUP BY ";
    		$sql.= implode(",",$this->groups);

    	}
    	if (isset($this->havings) && is_array($this->havings)) {
    		$sql.=" HAVING";
    		foreach ($this->havings as $wk => $having) {
    			$sql.=" $having[0] $having[1] $having[2] ";
    			if ($wk<count($this->havings)-1) {
    				$sql.=" ".strtoupper($having[3]);
    			}

    		}
    	}
    	if (isset($this->orders) && is_array($this->orders)) {
    		$sql.=" ORDER BY ";
    		foreach ($this->orders as $wk => $order) {
    			$sql.=" $order[0] $order[1]";
    			if ($wk<count($this->orders)-1) {
    				$sql.=" , ";
    			}
    		}
    	}
        
    	return $sql;
    }

    public function query($sql){
    	return $this->pdo->query($sql)->fetchAll($this->fetchType);
    }
    public function delete(){
        $sql = "DELETE FROM ".$this->from;

        if (isset($this->wheres) && is_array($this->wheres)) {
            $sql.=" WHERE";
            foreach ($this->wheres as $wk => $where) {
                $sql.=" $where[0] $where[1] $where[2] ";
                if ($wk<count($this->wheres)-1) {
                    $sql.=" ".strtoupper($where[3]);
                }

            }
        }
        // return $sql;
        $this->pdo->execute($sql);
    }
    public  function lastInsertedId(){
        return $this->pdo->lastInsertId();
    }
}